package sample.dialog;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import sample.DBConnection;
import sample.Klient;

import java.sql.SQLException;
import java.util.ArrayList;


public class GetKlientDialogController extends Dialog {

    @FXML
    Button buttonConfirm;
    @FXML
    Button buttonRefresh;
    @FXML
    Button buttonSearch;
    @FXML
    TextField textFieldSearch;

    @FXML
    TableView tableViewKlient;
    @FXML
    TableColumn columnPESELKlient;
    @FXML
    TableColumn columnImieKlient;
    @FXML
    TableColumn columnNazwiskoKlient;
    @FXML
    TableColumn columnNumerKlient;
    @FXML
    TableColumn columnEmailKlient;
    @FXML
    TableColumn columnNipKlient;



    String Klient_PESEL="NIE WYBRANO";

    @FXML
    private void initialize() {

        columnPESELKlient.setCellValueFactory(new PropertyValueFactory<>("PESEL"));
        columnImieKlient.setCellValueFactory(new PropertyValueFactory<>("Imie"));
        columnNazwiskoKlient.setCellValueFactory(new PropertyValueFactory<>("Nazwisko"));
        columnNumerKlient.setCellValueFactory(new PropertyValueFactory<>("Telefon"));
        columnEmailKlient.setCellValueFactory(new PropertyValueFactory<>("Email"));
        columnNipKlient.setCellValueFactory(new PropertyValueFactory<>("NIP"));

        buttonConfirm.setDisable(true);
        tableViewKlient.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                buttonConfirm.setDisable(false);
            }
            else {
                buttonConfirm.setDisable(true);
            }
        });

        textFieldSearch.textProperty().addListener((observable, oldValue, newValue) -> {
            try {
                updateTableViewSearchKlient();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    private void closeStage(ActionEvent event) {
        Node source = (Node)  event.getSource();
        Stage stage  = (Stage) source.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void buttonConfirmOnAction(ActionEvent event) {

        if(tableViewKlient.getSelectionModel().getSelectedItem() != null){
            int id = tableViewKlient.getSelectionModel().getFocusedIndex();
            Klient klient = (Klient) tableViewKlient.getItems().get(id);
            Klient_PESEL= klient.getPESEL();
        }

        closeStage(event);

    }

    @FXML
    private void buttonCancelOnAction(ActionEvent event) {
        closeStage(event);
    }

    private void updateTableViewKlient() throws SQLException, ClassNotFoundException {
        DBConnection dbConnection= new DBConnection();

        dbConnection.connect();

        ArrayList<Klient> listaK = Klient.getList(dbConnection,0);
        tableViewKlient.getItems().removeAll(tableViewKlient.getItems());
        for (Klient i:listaK) {
            tableViewKlient.getItems().add(i);
        }

        dbConnection.disconnect();
    }

    private void updateTableViewSearchKlient() throws SQLException {
        DBConnection dbConnection= new DBConnection();

        dbConnection.connect();

        ArrayList<Klient> listaK = Klient.searchFor(dbConnection,0,textFieldSearch.getText());
        tableViewKlient.getItems().removeAll(tableViewKlient.getItems());
        for (Klient i:listaK) {
            tableViewKlient.getItems().add(i);
        }

        dbConnection.disconnect();
    }

    @FXML
    public void buttonGetKlientOnAction() throws SQLException, ClassNotFoundException {

        updateTableViewKlient();

    }

    @FXML
    public void buttonSearchKlientOnAction() throws SQLException {
        updateTableViewSearchKlient();
    }


}
