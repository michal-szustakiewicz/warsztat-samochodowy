package sample.dialog;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import sample.Pojazd;
import sample.Pojazd;

public class NowyPojazdDialog {
    public Pojazd newPojazd(){
        try {
            Stage dialogStage = new Stage();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("NowyPojazdDialog.fxml"));
            AnchorPane root = loader.load();
            NowyPojazdDialogController controller = loader.getController();
            Scene scene = new Scene(root);
            dialogStage.setScene(scene);
            dialogStage.initModality(Modality.APPLICATION_MODAL);
            dialogStage.setTitle("Dodaj pojazd");
            dialogStage.setAlwaysOnTop(true);
            dialogStage.setResizable(false);
            dialogStage.showAndWait();
            if(controller.hasResult()) {
                return controller.getPojazd();
            }else {
                return null;
            }
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Pojazd editPojazd(Pojazd pojazd) {
        try {
            Stage dialogStage = new Stage();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("NowyPojazdDialog.fxml"));
            AnchorPane root = loader.load();
            NowyPojazdDialogController controller = loader.getController();
            Scene scene = new Scene(root);
            dialogStage.setScene(scene);
            controller.setPojazd(pojazd);
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.setTitle("Edytuj pojazd");
            dialogStage.setAlwaysOnTop(true);
            dialogStage.setResizable(false);
            dialogStage.showAndWait();
            if(controller.hasResult()) {
                return controller.getPojazd();
            }else {
                return null;
            }
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
