package sample.dialog;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import sample.*;

import java.sql.SQLException;
import java.util.ArrayList;


public class GetPojazdDialogController extends Dialog {

    @FXML
    Button buttonConfirm;
    @FXML
    Button buttonRefresh;
    @FXML
    Button buttonSearch;
    @FXML
    TextField textFieldSearch;

    @FXML
    TableView tableViewPojazd;
    @FXML
    TableColumn columnVINPojazd;
    @FXML
    TableColumn columnRejestracjaPojazd;
    @FXML
    TableColumn columnRokPojazd;
    @FXML
    TableColumn columnMarkaPojazd;
    @FXML
    TableColumn columnModelPojazd;
    @FXML
    TableColumn columnKlientPojazd;



    String Pojazd_VIN="NIE WYBRANO";

    @FXML
    private void initialize() {

        columnVINPojazd.setCellValueFactory(new PropertyValueFactory<>("VIN"));
        columnRejestracjaPojazd.setCellValueFactory(new PropertyValueFactory<>("Rejestracja"));
        columnRokPojazd.setCellValueFactory(new PropertyValueFactory<>("Rok"));
        columnMarkaPojazd.setCellValueFactory(new PropertyValueFactory<>("Marka"));
        columnModelPojazd.setCellValueFactory(new PropertyValueFactory<>("Model"));
        columnKlientPojazd.setCellValueFactory(new PropertyValueFactory<>("Klient_PESEL"));

        buttonConfirm.setDisable(true);
        tableViewPojazd.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                buttonConfirm.setDisable(false);
            }
            else {
                buttonConfirm.setDisable(true);
            }
        });

        textFieldSearch.textProperty().addListener((observable, oldValue, newValue) -> {
            try {
                updateTableViewSearch();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    private void closeStage(ActionEvent event) {
        Node source = (Node)  event.getSource();
        Stage stage  = (Stage) source.getScene().getWindow();
        stage.close();
    }

    private void updateTableView() throws SQLException, ClassNotFoundException {
        DBConnection dbConnection= new DBConnection();

        dbConnection.connect();

        ArrayList<Pojazd> listaPoj = Pojazd.getList(dbConnection,0);
        tableViewPojazd.getItems().removeAll(tableViewPojazd.getItems());
        for (Pojazd i:listaPoj) {
            tableViewPojazd.getItems().add(i);
        }

        dbConnection.disconnect();
    }



    private void updateTableViewSearch() throws SQLException{
        DBConnection dbConnection= new DBConnection();

        dbConnection.connect();

        ArrayList<Pojazd> listaPoj = Pojazd.searchFor(dbConnection,0,textFieldSearch.getText());
        tableViewPojazd.getItems().removeAll(tableViewPojazd.getItems());
        for (Pojazd i:listaPoj) {
            tableViewPojazd.getItems().add(i);
        }

        dbConnection.disconnect();
    }

    @FXML
    private void buttonConfirmOnAction(ActionEvent event) {

        if(tableViewPojazd.getSelectionModel().getSelectedItem() != null){
            int id = tableViewPojazd.getSelectionModel().getFocusedIndex();
            Pojazd pojazd = (Pojazd) tableViewPojazd.getItems().get(id);
            Pojazd_VIN= pojazd.getVIN();
        }

        closeStage(event);

    }

    @FXML
    private void buttonCancelOnAction(ActionEvent event) {
        closeStage(event);
    }



    @FXML
    public void buttonGetKlientOnAction() throws SQLException, ClassNotFoundException {

        updateTableView();

    }

    @FXML
    public void buttonSearchKlientOnAction() throws SQLException {
        updateTableViewSearch();
    }


}
