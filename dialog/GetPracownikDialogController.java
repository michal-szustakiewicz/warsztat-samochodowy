package sample.dialog;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import sample.*;

import java.sql.SQLException;
import java.util.ArrayList;


public class GetPracownikDialogController extends Dialog {

    @FXML
    Button buttonConfirm;
    @FXML
    Button buttonRefresh;
    @FXML
    Button buttonSearch;
    @FXML
    TextField textFieldSearch;

    @FXML
    TableView tableViewPracownik;
    @FXML
    TableColumn columnPESELPracownik;
    @FXML
    TableColumn columnImiePracownik;
    @FXML
    TableColumn columnNazwiskoPracownik;
    @FXML
    TableColumn columnNumerPracownik;
    @FXML
    TableColumn columnEmailPracownik;



    String Pracownik_PESEL="NIE WYBRANO";

    @FXML
    private void initialize() {

        columnPESELPracownik.setCellValueFactory(new PropertyValueFactory<>("PESEL"));
        columnImiePracownik.setCellValueFactory(new PropertyValueFactory<>("Imie"));
        columnNazwiskoPracownik.setCellValueFactory(new PropertyValueFactory<>("Nazwisko"));
        columnNumerPracownik.setCellValueFactory(new PropertyValueFactory<>("Telefon"));
        columnEmailPracownik.setCellValueFactory(new PropertyValueFactory<>("Email"));

        buttonConfirm.setDisable(true);
        tableViewPracownik.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                buttonConfirm.setDisable(false);
            }
            else {
                buttonConfirm.setDisable(true);
            }
        });

        textFieldSearch.textProperty().addListener((observable, oldValue, newValue) -> {
            try {
                updateTableViewSearch();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    private void closeStage(ActionEvent event) {
        Node source = (Node)  event.getSource();
        Stage stage  = (Stage) source.getScene().getWindow();
        stage.close();
    }

    private void updateTableView() throws SQLException, ClassNotFoundException {
        DBConnection dbConnection= new DBConnection();

        dbConnection.connect();

        ArrayList<Pracownik> listaP = Pracownik.getList(dbConnection,0);
        tableViewPracownik.getItems().removeAll(tableViewPracownik.getItems());
        for (Pracownik i:listaP) {
            tableViewPracownik.getItems().add(i);
        }

        dbConnection.disconnect();
    }

    private void updateTableViewSearch() throws SQLException{
        DBConnection dbConnection= new DBConnection();

        dbConnection.connect();

        ArrayList<Pracownik> listaK = Pracownik.searchFor(dbConnection,0,textFieldSearch.getText());
        tableViewPracownik.getItems().removeAll(tableViewPracownik.getItems());
        for (Pracownik i:listaK) {
            tableViewPracownik.getItems().add(i);
        }

        dbConnection.disconnect();
    }

    @FXML
    private void buttonConfirmOnAction(ActionEvent event) {

        if(tableViewPracownik.getSelectionModel().getSelectedItem() != null){
            int id = tableViewPracownik.getSelectionModel().getFocusedIndex();
            Pracownik pracownik = (Pracownik) tableViewPracownik.getItems().get(id);
            Pracownik_PESEL= pracownik.getPESEL();
        }

        closeStage(event);

    }

    @FXML
    private void buttonCancelOnAction(ActionEvent event) {
        closeStage(event);
    }


    @FXML
    public void buttonGetKlientOnAction() throws SQLException, ClassNotFoundException {

        updateTableView();

    }

    @FXML
    public void buttonSearchKlientOnAction() throws SQLException {
        updateTableViewSearch();
    }


}
