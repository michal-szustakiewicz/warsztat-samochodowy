package sample.dialog;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import sample.Klient;

public class LoginDialog {
    public boolean getLoginResult(){
        try {
            Stage dialogStage = new Stage();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("LoginDialog.fxml"));
            AnchorPane root = loader.load();
            LoginDialogController controller = loader.getController();
            Scene scene = new Scene(root);
            dialogStage.setScene(scene);
            dialogStage.initModality(Modality.APPLICATION_MODAL);
            dialogStage.setTitle("Logowanie");
            dialogStage.setAlwaysOnTop(true);
            dialogStage.setResizable(false);
            dialogStage.showAndWait();
            if(controller.hasResult()) {
                return controller.isLogged();
            }else {
                return false;
            }
        } catch(Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
