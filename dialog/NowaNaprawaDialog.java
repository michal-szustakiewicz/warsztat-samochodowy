package sample.dialog;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import sample.Naprawa;
import sample.Pojazd;

public class NowaNaprawaDialog {
    public Naprawa newNaprawa(){
        try {
            Stage dialogStage = new Stage();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("NowaNaprawaDialog.fxml"));
            AnchorPane root = loader.load();
            NowaNaprawaDialogController controller = loader.getController();
            Scene scene = new Scene(root);
            dialogStage.setScene(scene);
            dialogStage.initModality(Modality.APPLICATION_MODAL);
            dialogStage.setTitle("Dodaj naprawę");
            dialogStage.setAlwaysOnTop(true);
            dialogStage.setResizable(false);
            dialogStage.showAndWait();
            if(controller.hasResult()) {
                return controller.getNaprawa();
            }else {
                return null;
            }
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Naprawa editNaprawa(Naprawa naprawa) {
        try {
            Stage dialogStage = new Stage();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("NowaNaprawaDialog.fxml"));
            AnchorPane root = loader.load();
            NowaNaprawaDialogController controller = loader.getController();
            Scene scene = new Scene(root);
            dialogStage.setScene(scene);
            controller.setNaprawa(naprawa);
            dialogStage.initModality(Modality.APPLICATION_MODAL);
            dialogStage.setTitle("Edytuj naprawę");
            dialogStage.setAlwaysOnTop(true);
            dialogStage.setResizable(false);
            dialogStage.showAndWait();
            if(controller.hasResult()) {
                return controller.getNaprawa();
            }else {
                return null;
            }
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
