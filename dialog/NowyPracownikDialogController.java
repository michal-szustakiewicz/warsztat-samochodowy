package sample.dialog;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Dialog;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;
import sample.Pracownik;


public class NowyPracownikDialogController extends Dialog {

    @FXML
    private TextField textFieldPESELPracownik;
    @FXML
    private TextField textFieldImiePracownik;
    @FXML
    private TextField textFieldNazwiskoPracownik;
    @FXML
    private TextField textFieldNumerPracownik;
    @FXML
    private TextField textFieldEmailPracownik;

    private Pracownik pracownik;
    private boolean hasResult = false;
    String oldPESEL ="";

    @FXML
    private void initialize() {
        hasResult = false;
    }


    @FXML
    private void buttonConfirmOnAction(ActionEvent event) {
        if (isInputValid()) {

            String email;
            if(textFieldEmailPracownik.getText().isEmpty()) email = null;
            else email = textFieldEmailPracownik.getText();
            pracownik= new Pracownik(
                    textFieldPESELPracownik.getText(),
                    textFieldImiePracownik.getText(),
                    textFieldNazwiskoPracownik.getText(),
                    Integer.parseInt(textFieldNumerPracownik.getText()),
                    email);



            if(oldPESEL!="") pracownik.setOldPESEL(oldPESEL);
            else oldPESEL = textFieldPESELPracownik.getText();
            hasResult = true;
            closeStage(event);
        }
    }



    public boolean hasResult() {
        return hasResult;
    }

    public void setPracownik(Pracownik pracownik) {
        textFieldPESELPracownik.setText(pracownik.getPESEL());
        oldPESEL = pracownik.getPESEL();
        textFieldImiePracownik.setText(pracownik.getImie());
        textFieldNazwiskoPracownik.setText(pracownik.getNazwisko());
        textFieldNumerPracownik.setText(Integer.toString(pracownik.getTelefon()));
        textFieldEmailPracownik.setText(pracownik.getEmail());
    }

    public Pracownik getPracownik(){
        return pracownik;
    }

    private void closeStage(ActionEvent event) {
        Node source = (Node)  event.getSource();
        Stage stage  = (Stage) source.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void buttonCancelOnAction(ActionEvent event) {
        closeStage(event);
    }

    private boolean isInputValid(){

        String errorList = "";

        if(textFieldPESELPracownik.getText().isEmpty()) errorList += "PESEL: PESEL nie może być pusty\n";
        else if(textFieldPESELPracownik.getText().length() != 11) errorList += "PESEL: PESEL powinien mieć 11 cyfr\n";
        else if(!isNumeric(textFieldPESELPracownik.getText())) errorList += "PESEL: PESEL może zawierać tylko cyfry\n";

        if(textFieldImiePracownik.getText().isEmpty())errorList+= "Imię: Imię nie może być puste\n";
        if(textFieldImiePracownik.getText().length()>20)errorList+= "Imię: Imię jest za długie\n";

        if(textFieldNazwiskoPracownik.getText().isEmpty())errorList+= "Nazwisko: Nazwisko nie może być puste\n";
        if(textFieldNazwiskoPracownik.getText().length()>40)errorList+= "Nazwisko: Nazwisko jest za długie\n";


        if(textFieldNumerPracownik.getText().isEmpty()) errorList += "Numer: Numer nie może być pusty\n";
        else if(!isNumeric(textFieldNumerPracownik.getText())) errorList += "Numer: Numer może zawierać tylko cyfry\n";
        if(textFieldNumerPracownik.getText().length()>9) errorList += "Numer: Numer jest za długi\n";

        if(textFieldEmailPracownik.getText().length()>30) errorList += "Email: Email jest za długi\n";


        if(errorList != "") {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.getDialogPane().setMinWidth(700);
            alert.setTitle("Błąd");
            alert.setHeaderText("Nieprawidłowe dane:");
            alert.setContentText(errorList);
            alert.initModality(Modality.APPLICATION_MODAL);

            Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
            stage.setAlwaysOnTop(true);
            stage.toFront(); // not sure if necessary
            alert.showAndWait();
            return false;
        }
        else return true;
    }

    private boolean isNumeric(String text){
        try {
            Long.parseLong(text);
            return true;
        }
        catch (NumberFormatException e)
        {
            return false;
        }
    }

}
