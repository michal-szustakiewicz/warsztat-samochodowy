package sample.dialog;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import sample.Klient;

public class GetKlientDialog {
    public String getPESEL(){
        try {
            Stage dialogStage = new Stage();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("GetKlientDialog.fxml"));
            AnchorPane root = loader.load();
            GetKlientDialogController controller = loader.getController();
            Scene scene = new Scene(root);
            dialogStage.setScene(scene);
            dialogStage.initModality(Modality.APPLICATION_MODAL);
            dialogStage.setTitle("Wybierz klienta");
            dialogStage.setAlwaysOnTop(true);
            dialogStage.setResizable(false);
            dialogStage.showAndWait();
            return controller.Klient_PESEL;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }


}
