package sample.dialog;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.stage.Modality;
import javafx.stage.Stage;
import sample.*;

import java.sql.SQLException;


public class NowyPojazdDialogController extends Dialog {

    @FXML
    private TextField textFieldVIN;
    @FXML
    private TextField textFieldRejestracja;
    @FXML
    private TextField textFieldRok;
    @FXML
    private TextField textFieldMarka;
    @FXML
    private TextField textFieldModel;
    @FXML
    private TextField textFieldKlientPESEL;
    @FXML
    Button buttonGetKlient;



    private Pojazd pojazd;
    private boolean hasResult = false;
    String oldVin ="";

    @FXML
    private void initialize() throws SQLException, ClassNotFoundException {
        hasResult = false;
        textFieldKlientPESEL.setDisable(true);
    }


    @FXML
    private void buttonConfirmOnAction(ActionEvent event) {
        if (isInputValid()) {
            pojazd= new Pojazd(
                    textFieldVIN.getText(),
                    textFieldRejestracja.getText(),
                    Integer.parseInt(textFieldRok.getText()),
                    textFieldMarka.getText(),
                    textFieldModel.getText(),
                    textFieldKlientPESEL.getText());



            if(oldVin!="") pojazd.setOldVIN(oldVin);
            else oldVin = textFieldVIN.getText();
            hasResult = true;
            closeStage(event);
        }
    }



    public boolean hasResult() {
        return hasResult;
    }

    public void setPojazd(Pojazd pojazd) {
        textFieldVIN.setText(pojazd.getVIN());
        oldVin = pojazd.getVIN();
        textFieldRejestracja.setText(pojazd.getRejestracja());
        textFieldRok.setText(Integer.toString(pojazd.getRok()));
        textFieldMarka.setText(pojazd.getMarka());
        textFieldModel.setText(pojazd.getModel());
        textFieldKlientPESEL.setText(pojazd.getKlient_PESEL());
    }

    public Pojazd getPojazd(){
        return pojazd;
    }

    private void closeStage(ActionEvent event) {
        Node source = (Node)  event.getSource();
        Stage stage  = (Stage) source.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void buttonCancelOnAction(ActionEvent event) {
        closeStage(event);
    }

    @FXML
    private void buttonGetKlientOnAction(ActionEvent event) {
        GetKlientDialog getKlientDialog = new GetKlientDialog();
        String klient_PESEL = getKlientDialog.getPESEL();
        textFieldKlientPESEL.setText(klient_PESEL);
    }

    private boolean isInputValid(){

        String errorList = "";

        if(textFieldVIN.getText().isEmpty()) errorList += "VIN: VIN nie może być pusty\n";
        else if(textFieldVIN.getText().length() != 17) errorList += "VIN: VIN powinien mieć 17 cyfr\n";

        if(textFieldRejestracja.getText().isEmpty())errorList+= "Rejestracja: Rejestracja nie może być pusta\n";
        if(textFieldRejestracja.getText().length()>8)errorList+= "Rejestracja: Rejestracja jest za długa\n";

        if(textFieldRok.getText().isEmpty())errorList+= "Rok: Rok nie może być pusty\n";
        if(textFieldRok.getText().length()!=4)errorList+= "Rok: Rok powinien zawierać 4 cyfry np. 1995\n";
        if(!isNumeric(textFieldRok.getText()))errorList+= "Rok: Rok może zawierać tylko cyfry np. 1995\n";

        if(textFieldMarka.getText().isEmpty()) errorList += "Marka: Marka nie może być pusta\n";
        if(textFieldMarka.getText().length()>20) errorList += "Marka: Marka jest za długa\n";

        if(textFieldModel.getText().isEmpty()) errorList += "Model: Model nie może być pusty\n";
        if(textFieldModel.getText().length()>20) errorList += "Model: Model jest za długi\n";




        if(errorList != "") {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.getDialogPane().setMinWidth(700);
            alert.setTitle("Błąd");
            alert.setHeaderText("Nieprawidłowe dane:");
            alert.setContentText(errorList);
            alert.initModality(Modality.APPLICATION_MODAL);

            Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
            stage.setAlwaysOnTop(true);
            stage.toFront(); // not sure if necessary
            alert.showAndWait();
            return false;
        }
        else return true;
    }

    private boolean isNumeric(String text){
        try {
            Long.parseLong(text);
            return true;
        }
        catch (NumberFormatException e)
        {
            return false;
        }
    }

}
