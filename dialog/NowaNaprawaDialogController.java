package sample.dialog;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import sample.DBConnection;
import sample.Naprawa;

import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;


public class NowaNaprawaDialogController extends Dialog {

    @FXML
    TextField textFieldPojazdVIN;
    @FXML
    TextField textFieldKosztNapraw;
    @FXML
    TextField textFieldKosztCzesci;
    @FXML
    TextArea textAreaPrzeprowadzoneNaprawy;
    @FXML
    TextArea textAreaZgloszoneUsterki;
    @FXML
    TextField textFieldPaliwoPrzed;
    @FXML
    TextField textFieldPaliwoPo;
    @FXML
    TextField textFieldPracownikPesel;
    @FXML
    DatePicker datePickerDataPrzyjecia;
    @FXML
    DatePicker datePickerDataOddania;
    @FXML
    Button buttonConfirmNaprawy;
    @FXML
    Button buttonCancelPracownik;
    @FXML
    Button buttonGetPojazd;




    private Naprawa naprawa;
    private int ID = 0;
    private boolean hasResult = false;


    public Naprawa getNaprawa() {
        return naprawa;
    }
    public int getID() {
        return ID;
    }
    public void setID(int ID) {
        this.ID = ID;
    }

    @FXML
    private void initialize() {
        textFieldPojazdVIN.setDisable(true);
        hasResult = false;
        StringConverter<LocalDate> converter = new StringConverter<>() {
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

            @Override
            public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }

            @Override
            public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        };


        datePickerDataPrzyjecia.setConverter(converter);
        datePickerDataPrzyjecia.getEditor().setDisable(true);
        datePickerDataPrzyjecia.getEditor().setStyle("-fx-opacity: 1.0;");
        datePickerDataPrzyjecia.setValue(null);
        datePickerDataOddania.setConverter(converter);
        datePickerDataOddania.getEditor().setDisable(true);
        datePickerDataOddania.getEditor().setStyle("-fx-opacity: 1.0;");
        datePickerDataOddania.setValue(null);
    }

    @FXML
    private void buttonConfirmOnAction(ActionEvent event) throws SQLException {
        if (isInputValid()) {

            Date sqlData_Przyjecia = Date.valueOf(datePickerDataPrzyjecia.getValue().toString());
            System.out.println(sqlData_Przyjecia.toString());
            Date sqlData_Oddania = null;
            if(datePickerDataOddania.getValue() != null){
                sqlData_Oddania = Date.valueOf(datePickerDataOddania.getValue().toString());
            }
            String przeprowadzone_naprawy;
            if(textAreaPrzeprowadzoneNaprawy.getText().isEmpty()) przeprowadzone_naprawy = null;
            else przeprowadzone_naprawy = "'" + textAreaPrzeprowadzoneNaprawy.getText() + "'";


            naprawa= new Naprawa(
                    ID,
                    textFieldPojazdVIN.getText(),
                    sqlData_Przyjecia,
                    sqlData_Oddania,
                    textAreaZgloszoneUsterki.getText(),
                    przeprowadzone_naprawy,
                    DBConnection.FloatFromString(textFieldKosztNapraw.getText()),
                    DBConnection.FloatFromString(textFieldKosztCzesci.getText()),
                    Integer.parseInt(textFieldPaliwoPrzed.getText()),
                    DBConnection.IntegerFromString(textFieldPaliwoPo.getText()),
                    textFieldPracownikPesel.getText());

            hasResult = true;
            closeStage(event);
        }
    }

    public boolean hasResult() {
        return hasResult;
    }

    public void setNaprawa(Naprawa naprawa) {
        ID = naprawa.getIdNaprawa();
        textFieldPojazdVIN.setText(naprawa.getPojazd_VIN());
        datePickerDataPrzyjecia.setValue(naprawa.getData_przyjecia().toLocalDate());
        if(naprawa.getData_oddania() != null) datePickerDataOddania.setValue(naprawa.getData_oddania().toLocalDate());
        textAreaZgloszoneUsterki.setText(naprawa.getZgłoszone_usterki());
        if(naprawa.getPrzeprowadzone_naprawy() != null) textAreaPrzeprowadzoneNaprawy.setText(naprawa.getPrzeprowadzone_naprawy());
        if(naprawa.getKoszt_napraw() != null) textFieldKosztNapraw.setText(naprawa.getKoszt_napraw().toString());
        if(naprawa.getKoszt_czesci() != null) textFieldKosztCzesci.setText(naprawa.getKoszt_czesci().toString());
        textFieldPaliwoPrzed.setText(Integer.toString(naprawa.getPaliwo_przed()));
        if(naprawa.getPaliwo_po() != null) textFieldPaliwoPo.setText(naprawa.getPaliwo_po().toString());
        textFieldPracownikPesel.setText(naprawa.getPracownik_PESEL());
    }

    private void closeStage(ActionEvent event) {
        Node source = (Node)  event.getSource();
        Stage stage  = (Stage) source.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void buttonCancelOnAction(ActionEvent event) {
        closeStage(event);
    }

    @FXML
    private void buttonClearDataOddaniaOnAction(ActionEvent event) {

        datePickerDataOddania.setValue(null);
    }

    @FXML
    private void buttonGetPojazdOnAction(ActionEvent event) {
        GetPojazdDialog getPojazdDialog = new GetPojazdDialog();
        String Pojazd_VIN = getPojazdDialog.getVIN();
        textFieldPojazdVIN.setText(Pojazd_VIN);
    }
    @FXML
    private void buttonGetPracownikOnAction(ActionEvent event) {
        GetPracownikDialog getPracownikDialog = new GetPracownikDialog();
        String Pracownik_PESEL = getPracownikDialog.getPESEL();
        textFieldPracownikPesel.setText(Pracownik_PESEL);
    }


    private boolean isInputValid(){

        String errorList = "";

        if(datePickerDataOddania.getValue() != null && datePickerDataPrzyjecia.getValue().isAfter(datePickerDataOddania.getValue()))
            errorList += "Data oddania: Data oddania powinna być większa bądź równa dacie przyjęcia\n";
        if(!textFieldKosztCzesci.getText().isEmpty()) {
            if(!isFloat(textFieldKosztCzesci.getText())) {
                errorList += "Koszt części: Koszt części powinien być liczbą zmienno przecinkową np 100.99\n";
            }
        }
        if(!textFieldKosztNapraw.getText().isEmpty()) {
            if(!isFloat(textFieldKosztNapraw.getText())) {
                errorList += "Koszt napraw: Koszt napraw powinien być liczbą zmienno przecinkową np 100.99\n";
            }
        }
        if(!textFieldPaliwoPo.getText().isEmpty()) {
            if(!isNumeric(textFieldPaliwoPo.getText())) {
                errorList += "Paliwo po: Paliwo po powinno być liczbą całkowitą z zakresu 0 - 100\n";
            }
            else if(Integer.parseInt(textFieldPaliwoPo.getText())<0 || Integer.parseInt(textFieldPaliwoPo.getText())>100)
                 errorList += "Paliwo po: Paliwo po powinno być liczbą całkowitą z zakresu 0 - 100\n";
        }
        if(textFieldPaliwoPrzed.getText().isEmpty()) {
            errorList += "Paliwo przed: Paliwo przed nie może być puste";
        }
        else if(!isNumeric(textFieldPaliwoPrzed.getText())) {
            errorList += "Paliwo przed: Paliwo przed powinno być liczbą całkowitą z zakresu 0 - 100\n";
        }
        else if(Integer.parseInt(textFieldPaliwoPrzed.getText())<0 || Integer.parseInt(textFieldPaliwoPrzed.getText())>100)
            errorList += "Paliwo przed: Paliwo przed powinno być liczbą całkowitą z zakresu 0 - 100\n";






        if(errorList != "") {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.getDialogPane().setMinWidth(700);
            alert.setTitle("Błąd");
            alert.setHeaderText("Nieprawidłowe dane:");
            alert.setContentText(errorList);
            alert.initModality(Modality.APPLICATION_MODAL);

            Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
            stage.setAlwaysOnTop(true);
            stage.toFront(); // not sure if necessary
            alert.showAndWait();
            return false;
        }
        else return true;


    }

    private boolean isNumeric(String text){
        try {
            Long.parseLong(text);
            return true;
        }
        catch (NumberFormatException e)
        {
            return false;
        }
    }

    private boolean isFloat(String text){
        try {
            Float.parseFloat(text);
            return true;
        }
        catch (Exception e)
        {
            return false;
        }
    }


}
