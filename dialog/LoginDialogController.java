package sample.dialog;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Dialog;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import sample.DBConnection;
import sample.Klient;

public class LoginDialogController extends Dialog {

    @FXML
    private TextField textFieldLogin;
    @FXML
    private TextField passwordFieldPassword;
    @FXML
    private Button buttonConfirm;

    private boolean isLogged = false;
    private boolean hasResult = false;

    public boolean isLogged() {
        return isLogged;
    }

    @FXML
    private void initialize() {
        hasResult = false;
        buttonConfirm.setDefaultButton(true);
        this.getDialogPane().getScene().setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                switch (event.getCode()) {
                    case ENTER: buttonConfirm.fire(); break;
                }
            }
        });
    }

    @FXML
    private void buttonConfirmOnAction(ActionEvent event) {
        if (DBConnection.setConnectionString(textFieldLogin.getText(),passwordFieldPassword.getText())) {
            hasResult = true;
            isLogged = true;
            closeStage(event);
        } else {

            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.getDialogPane().setMinWidth(700);
            alert.setTitle("Błąd");
            alert.setHeaderText("Nieprawidłowe dane:");
            alert.setContentText("Nieprawidłowy login lub hasło");
            alert.initModality(Modality.APPLICATION_MODAL);
            Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
            stage.setAlwaysOnTop(true);
            stage.toFront(); // not sure if necessary
            alert.showAndWait();

        }
    }

    public boolean hasResult() {
        return hasResult;
    }

    private void closeStage(ActionEvent event) {
        Node source = (Node)  event.getSource();
        Stage stage  = (Stage) source.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void buttonCancelOnAction(ActionEvent event) {
        closeStage(event);
    }
}
