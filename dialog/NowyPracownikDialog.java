package sample.dialog;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import sample.Pracownik;

public class NowyPracownikDialog {
    public Pracownik newPracownik(){
        try {
            Stage dialogStage = new Stage();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("NowyPracownikDialog.fxml"));
            AnchorPane root = loader.load();
            NowyPracownikDialogController controller = loader.getController();
            Scene scene = new Scene(root);
            dialogStage.setScene(scene);
            dialogStage.initModality(Modality.APPLICATION_MODAL);
            dialogStage.setTitle("Dodaj pracownika");
            dialogStage.setAlwaysOnTop(true);
            dialogStage.setResizable(false);
            dialogStage.showAndWait();
            if(controller.hasResult()) {
                return controller.getPracownik();
            }else {
                return null;
            }
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Pracownik editPracownik(Pracownik pracownik) {
        try {
            Stage dialogStage = new Stage();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("NowyPracownikDialog.fxml"));
            AnchorPane root = loader.load();
            NowyPracownikDialogController controller = loader.getController();
            Scene scene = new Scene(root);
            dialogStage.setScene(scene);
            controller.setPracownik(pracownik);
            dialogStage.initModality(Modality.APPLICATION_MODAL);
            dialogStage.setTitle("Edytuj pracownika");
            dialogStage.setAlwaysOnTop(true);
            dialogStage.setResizable(false);
            dialogStage.showAndWait();
            if(controller.hasResult()) {
                return controller.getPracownik();
            }else {
                return null;
            }
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
