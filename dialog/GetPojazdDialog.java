package sample.dialog;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class GetPojazdDialog {
    public String getVIN(){
        try {
            Stage dialogStage = new Stage();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("GetPojazdDialog.fxml"));
            AnchorPane root = loader.load();
            GetPojazdDialogController controller = loader.getController();
            Scene scene = new Scene(root);
            dialogStage.setScene(scene);
            dialogStage.initModality(Modality.APPLICATION_MODAL);
            dialogStage.setTitle("Wybierz pojazd");
            dialogStage.setAlwaysOnTop(true);
            dialogStage.setResizable(false);
            dialogStage.showAndWait();
            return controller.Pojazd_VIN;
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }


}
