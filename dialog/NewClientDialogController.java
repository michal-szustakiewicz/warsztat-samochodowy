package sample.dialog;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Dialog;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.stage.Modality;
import javafx.stage.Stage;
import sample.Klient;

import java.text.NumberFormat;


public class NewClientDialogController extends Dialog {

    @FXML
    private TextField textFieldPESEL;
    @FXML
    private TextField textFieldImie;
    @FXML
    private TextField textFieldNazwisko;
    @FXML
    private TextField textFieldNumer;
    @FXML
    private TextField textFieldEmail;
    @FXML
    private TextField textFieldNIP;

    private Klient klient;
    private boolean hasResult = false;
    String oldPESEL ="";

    @FXML
    private void initialize() {
        hasResult = false;
    }


    @FXML
    private void buttonConfirmOnAction(ActionEvent event) {
        if (isInputValid()) {

            String email;
            if(textFieldEmail.getText().isEmpty()) email = null;
            else email = "'"+textFieldEmail.getText()+"'";
            String nip;
            if(textFieldNIP.getText().isEmpty()) nip = null;
            else nip = "'"+textFieldNIP.getText()+"'";


            klient= new Klient(
                    textFieldPESEL.getText(),
                    textFieldImie.getText(),
                    textFieldNazwisko.getText(),
                    Integer.parseInt(textFieldNumer.getText()),
                    email,
                    nip);



            if(oldPESEL!="") klient.setOldPESEL(oldPESEL);
            else oldPESEL = textFieldPESEL.getText();
            hasResult = true;
            closeStage(event);
        }
    }



    public boolean hasResult() {
        return hasResult;
    }

    public Klient getKlient(){
        return klient;
    }

    private void closeStage(ActionEvent event) {
        Node source = (Node)  event.getSource();
        Stage stage  = (Stage) source.getScene().getWindow();
        stage.close();
    }



    @FXML
    private void buttonCancelOnAction(ActionEvent event) {
        closeStage(event);
    }

    private boolean isInputValid(){

        String errorList = "";

        if(textFieldPESEL.getText().isEmpty()) errorList += "PESEL: PESEL nie może być pusty\n";
        else if(textFieldPESEL.getText().length() != 11) errorList += "PESEL: PESEL powinien mieć 11 cyfr\n";
        else if(!isNumeric(textFieldPESEL.getText())) errorList += "PESEL: PESEL może zawierać tylko cyfry\n";

        if(textFieldImie.getText().isEmpty())errorList+= "Imię: Imię nie może być puste\n";
        if(textFieldImie.getText().length()>20)errorList+= "Imię: Imię jest za długie\n";

        if(textFieldNazwisko.getText().isEmpty())errorList+= "Nazwisko: Nazwisko nie może być puste\n";
        if(textFieldNazwisko.getText().length()>40)errorList+= "Nazwisko: Nazwisko jest za długie\n";


        if(textFieldNumer.getText().isEmpty()) errorList += "Numer: Numer nie może być pusty\n";
        else if(!isNumeric(textFieldNumer.getText())) errorList += "Numer: Numer może zawierać tylko cyfry\n";
        if(textFieldNumer.getText().length()>9) errorList += "Numer: Numer jest za długi\n";

        if(!textFieldEmail.getText().isEmpty()){
            if(textFieldEmail.getText().length()>30) errorList += "Email: Email jest za długi\n";
        }

        if(!textFieldNIP.getText().isEmpty()){
            if(textFieldNIP.getText().length()!=10) errorList += "NIP: NIP musi mieć 10 znaków\n";
        }






        if(errorList != "") {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.getDialogPane().setMinWidth(700);
            alert.setTitle("Błąd");
            alert.setHeaderText("Nieprawidłowe dane:");
            alert.setContentText(errorList);
            alert.initModality(Modality.APPLICATION_MODAL);

            Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
            stage.setAlwaysOnTop(true);
            stage.toFront(); // not sure if necessary
            alert.showAndWait();
            return false;
        }
        else return true;
    }

    private boolean isNumeric(String text){
        try {
            Long.parseLong(text);
            return true;
        }
        catch (NumberFormatException e)
        {
            return false;
        }
    }

    public void setKlient(Klient klient) {
        textFieldPESEL.setText(klient.getPESEL());
        oldPESEL = klient.getPESEL();
        //textFieldPESEL.setDisable(true);
        textFieldImie.setText(klient.getImie());
        textFieldNazwisko.setText(klient.getNazwisko());
        textFieldNumer.setText(Integer.toString(klient.getTelefon()));
        if(klient.getEmail() != null)textFieldEmail.setText(klient.getEmail());
        if(klient.getNIP() != null) textFieldNIP.setText(klient.getNIP());


    }
}
