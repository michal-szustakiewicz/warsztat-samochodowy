package sample;


import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.util.StringConverter;
import sample.dialog.NewClientDialog;
import sample.dialog.NowaNaprawaDialog;
import sample.dialog.NowyPojazdDialog;
import sample.dialog.NowyPracownikDialog;


import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Optional;

public class mainwindowController {

    @FXML
    TabPane tabPane;
    @FXML
    Tab tabKlient;
    @FXML
    Tab tabPojazd;
    @FXML
    Tab tabNaprawa;
    @FXML
    Tab tabPracownik;


    //Kontrolki KLIENT://////////////
    @FXML
    Button buttonGetKlient;
    @FXML
    Button buttonAddKlient;
    @FXML
    Button buttonEditKlient;
    @FXML
    Button buttonDeleteKlient;
    @FXML
    Button buttonSearchKlient;
    @FXML
    TextField textFieldSearchKlient;
    @FXML
    Button buttonShowVehiclesKlient;
    @FXML
    Button buttonShowRepairsKlient;

    @FXML
    TableView tableViewKlient;
    @FXML
    TableColumn columnPESELKlient;
    @FXML
    TableColumn columnImieKlient;
    @FXML
    TableColumn columnNazwiskoKlient;
    @FXML
    TableColumn columnNumerKlient;
    @FXML
    TableColumn columnEmailKlient;
    @FXML
    TableColumn columnNipKlient;

    //Kontrolki Pracownik://////////////
    @FXML
    Button buttonGetPracownik;
    @FXML
    Button buttonAddPracownik;
    @FXML
    Button buttonEditPracownik;
    @FXML
    Button buttonDeletePracownik;
    @FXML
    Button buttonSearchPracownik;
    @FXML
    TextField textFieldSearchPracownik;
    @FXML
    Button buttonShowRepairsPracownik;

    @FXML
    TableView tableViewPracownik;
    @FXML
    TableColumn columnPESELPracownik;
    @FXML
    TableColumn columnImiePracownik;
    @FXML
    TableColumn columnNazwiskoPracownik;
    @FXML
    TableColumn columnNumerPracownik;
    @FXML
    TableColumn columnEmailPracownik;

    //Kontrolki Pojazd://////////////

    @FXML
    Button buttonGetPojazd;
    @FXML
    Button buttonAddPojazd;
    @FXML
    Button buttonEditPojazd;
    @FXML
    Button buttonDeletePojazd;
    @FXML
    Button buttonSearchPojazd;
    @FXML
    TextField textFieldSearchPojazd;
    @FXML
    Button buttonShowNaprawaPojazd;
    @FXML
    Button buttonShowKlientPojazd;

    @FXML
    TableView tableViewPojazd;
    @FXML
    TableColumn columnVINPojazd;
    @FXML
    TableColumn columnRejestracjaPojazd;
    @FXML
    TableColumn columnRokPojazd;
    @FXML
    TableColumn columnMarkaPojazd;
    @FXML
    TableColumn columnModelPojazd;
    @FXML
    TableColumn columnKlientPojazd;

    //Kontrolki Naprawa://////////////

    @FXML
    Button buttonGetNaprawa;
    @FXML
    Button buttonAddNaprawa;
    @FXML
    Button buttonEditNaprawa;
    @FXML
    Button buttonDeleteNaprawa;
    @FXML
    Button buttonSearchNaprawa;
    @FXML
    DatePicker datePickerFromNaprawa;
    @FXML
    DatePicker datePickerToNaprawa;
    @FXML
    Button buttonCleardatePickerFromNaprawa;
    @FXML
    Button buttonCleardatePickerToNaprawa;
    @FXML
    Button buttonShowPojazdNaprawa;
    @FXML
    Button buttonShowKlientNaprawa;
    @FXML
    Button buttonShowPracownikNaprawa;
    @FXML
    Button buttonShowNaprawaSzczegoly;

    @FXML
    TableView tableViewNaprawa;
    @FXML
    TableColumn columnIDNaprawa;
    @FXML
    TableColumn columnImieKlientNaprawa;
    @FXML
    TableColumn columnNazwiskoKlientNaprawa;
    @FXML
    TableColumn columnRejestracjaNaprawa;
    @FXML
    TableColumn columnMarkaNaprawa;
    @FXML
    TableColumn columnModelNaprawa;
    @FXML
    TableColumn columnData_przyjeciaNaprawa;
    @FXML
    TableColumn columnData_oddaniaNaprawa;

    @FXML
    void initialize() {

        //KLIENT///////////////////////////////////////////////////////////////////////////////////////////////

        columnPESELKlient.setCellValueFactory(new PropertyValueFactory<>("PESEL"));
        columnImieKlient.setCellValueFactory(new PropertyValueFactory<>("Imie"));
        columnNazwiskoKlient.setCellValueFactory(new PropertyValueFactory<>("Nazwisko"));
        columnNumerKlient.setCellValueFactory(new PropertyValueFactory<>("Telefon"));
        columnEmailKlient.setCellValueFactory(new PropertyValueFactory<>("Email"));
        columnNipKlient.setCellValueFactory(new PropertyValueFactory<>("NIP"));

        buttonEditKlient.setDisable(true);
        buttonDeleteKlient.setDisable(true);
        buttonShowVehiclesKlient.setDisable(true);
        buttonShowRepairsKlient.setDisable(true);

        tableViewKlient.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                buttonEditKlient.setDisable(false);
                buttonDeleteKlient.setDisable(false);
                buttonShowVehiclesKlient.setDisable(false);
                buttonShowRepairsKlient.setDisable(false);
            }
            else {
                buttonEditKlient.setDisable(true);
                buttonDeleteKlient.setDisable(true);
                buttonShowVehiclesKlient.setDisable(true);
                buttonShowRepairsKlient.setDisable(true);
            }
        });

        textFieldSearchKlient.textProperty().addListener((observable, oldValue, newValue) -> {
            try {
                updateTableViewSearchKlient();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });

        //Pracownik//////////////////////////////////////////////////////////////////////////////////////////////////////////

        columnPESELPracownik.setCellValueFactory(new PropertyValueFactory<>("PESEL"));
        columnImiePracownik.setCellValueFactory(new PropertyValueFactory<>("Imie"));
        columnNazwiskoPracownik.setCellValueFactory(new PropertyValueFactory<>("Nazwisko"));
        columnNumerPracownik.setCellValueFactory(new PropertyValueFactory<>("Telefon"));
        columnEmailPracownik.setCellValueFactory(new PropertyValueFactory<>("Email"));

        buttonEditPracownik.setDisable(true);
        buttonDeletePracownik.setDisable(true);
        buttonShowRepairsPracownik.setDisable(true);

        tableViewPracownik.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                buttonEditPracownik.setDisable(false);
                buttonDeletePracownik.setDisable(false);
                buttonShowRepairsPracownik.setDisable(false);
            }
            else {
                buttonEditPracownik.setDisable(true);
                buttonDeletePracownik.setDisable(true);
                buttonShowRepairsPracownik.setDisable(true);
            }
        });

        textFieldSearchPracownik.textProperty().addListener((observable, oldValue, newValue) -> {
            try {
                updateTableViewSearchPracownik();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });

        //Pojazd/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        columnVINPojazd.setCellValueFactory(new PropertyValueFactory<>("VIN"));
        columnRejestracjaPojazd.setCellValueFactory(new PropertyValueFactory<>("Rejestracja"));
        columnRokPojazd.setCellValueFactory(new PropertyValueFactory<>("Rok"));
        columnMarkaPojazd.setCellValueFactory(new PropertyValueFactory<>("Marka"));
        columnModelPojazd.setCellValueFactory(new PropertyValueFactory<>("Model"));
        columnKlientPojazd.setCellValueFactory(new PropertyValueFactory<>("Klient_PESEL"));

        buttonEditPojazd.setDisable(true);
        buttonDeletePojazd.setDisable(true);
        buttonShowNaprawaPojazd.setDisable(true);
        buttonShowKlientPojazd.setDisable(true);

        tableViewPojazd.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                buttonEditPojazd.setDisable(false);
                buttonDeletePojazd.setDisable(false);
                buttonShowNaprawaPojazd.setDisable(false);
                buttonShowKlientPojazd.setDisable(false);
            }
            else {
                buttonEditPojazd.setDisable(true);
                buttonDeletePojazd.setDisable(true);
                buttonShowNaprawaPojazd.setDisable(true);
                buttonShowKlientPojazd.setDisable(true);
            }
        });

        textFieldSearchPojazd.textProperty().addListener((observable, oldValue, newValue) -> {
            try {
                updateTableViewSearchPojazd();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });

        //Naprawa/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        columnIDNaprawa.setCellValueFactory(new PropertyValueFactory<>("idNaprawa"));
        columnImieKlientNaprawa.setCellValueFactory(new PropertyValueFactory<>("Imie"));
        columnNazwiskoKlientNaprawa.setCellValueFactory(new PropertyValueFactory<>("Nazwisko"));
        columnRejestracjaNaprawa.setCellValueFactory(new PropertyValueFactory<>("Rejestracja"));
        columnMarkaNaprawa.setCellValueFactory(new PropertyValueFactory<>("Marka"));
        columnModelNaprawa.setCellValueFactory(new PropertyValueFactory<>("Model"));
        columnData_przyjeciaNaprawa.setCellValueFactory(new PropertyValueFactory<>("Data_przyjecia"));
        columnData_oddaniaNaprawa.setCellValueFactory(new PropertyValueFactory<>("Data_oddania"));

        buttonEditNaprawa.setDisable(true);
        buttonDeleteNaprawa.setDisable(true);
        buttonShowPojazdNaprawa.setDisable(true);
        buttonShowPracownikNaprawa.setDisable(true);
        buttonShowKlientNaprawa.setDisable(true);
        buttonShowNaprawaSzczegoly.setDisable(true);

        tableViewNaprawa.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                buttonEditNaprawa.setDisable(false);
                buttonDeleteNaprawa.setDisable(false);
                buttonShowPojazdNaprawa.setDisable(false);
                buttonShowPracownikNaprawa.setDisable(false);
                buttonShowKlientNaprawa.setDisable(false);
                buttonShowNaprawaSzczegoly.setDisable(false);
            }
            else {
                buttonEditNaprawa.setDisable(true);
                buttonDeleteNaprawa.setDisable(true);
                buttonShowPojazdNaprawa.setDisable(true);
                buttonShowPracownikNaprawa.setDisable(true);
                buttonShowKlientNaprawa.setDisable(true);
                buttonShowNaprawaSzczegoly.setDisable(true);
            }
        });
        String pattern = "yyyy-MM-dd";
        StringConverter<LocalDate> converter = new StringConverter<>() {
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override
            public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }

            @Override
            public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        };

        datePickerFromNaprawa.setConverter(converter);
        datePickerFromNaprawa.getEditor().setDisable(true);
        datePickerFromNaprawa.getEditor().setStyle("-fx-opacity: 1.0;");
        datePickerToNaprawa.setValue(null);
        datePickerToNaprawa.setConverter(converter);
        datePickerToNaprawa.getEditor().setDisable(true);
        datePickerToNaprawa.getEditor().setStyle("-fx-opacity: 1.0;");
        datePickerToNaprawa.setValue(null);


    }

    /**KLIENT*/

    private void updateTableViewKlient() throws SQLException, ClassNotFoundException {
        DBConnection dbConnection= new DBConnection();

        dbConnection.connect();

        ArrayList<Klient> listaK = Klient.getList(dbConnection,0);
        tableViewKlient.getItems().removeAll(tableViewKlient.getItems());
        for (Klient i:listaK) {
            tableViewKlient.getItems().add(i);
        }

        dbConnection.disconnect();
    }

    private void updateTableViewSearchKlient() throws SQLException {
        DBConnection dbConnection= new DBConnection();

        dbConnection.connect();

        ArrayList<Klient> listaK = Klient.searchFor(dbConnection,0,textFieldSearchKlient.getText());
        tableViewKlient.getItems().removeAll(tableViewKlient.getItems());
        for (Klient i:listaK) {
            tableViewKlient.getItems().add(i);
        }

        dbConnection.disconnect();
    }

    private void updateTableViewNaprawaFromKlient() throws SQLException{
        DBConnection dbConnection= new DBConnection();

        dbConnection.connect();

        int id = tableViewKlient.getSelectionModel().getFocusedIndex();
        Klient klient = (Klient) tableViewKlient.getItems().get(id);
        ArrayList<NaprawaUproszczony> listaN = Klient.getNaprawy(dbConnection,klient.getPESEL(),0);
        tableViewNaprawa.getItems().removeAll(tableViewNaprawa.getItems());
        for (NaprawaUproszczony i:listaN) {
            tableViewNaprawa.getItems().add(i);
        }


        dbConnection.disconnect();

    }

    private void updateTableViewPojazdFromKlient() throws SQLException{
        DBConnection dbConnection= new DBConnection();

        dbConnection.connect();

        int id = tableViewKlient.getSelectionModel().getFocusedIndex();
        Klient klient = (Klient) tableViewKlient.getItems().get(id);
        ArrayList<Pojazd> listaN = Klient.getPojazdy(dbConnection,klient.getPESEL(),0);
        tableViewPojazd.getItems().removeAll(tableViewPojazd.getItems());
        for (Pojazd i:listaN) {
            tableViewPojazd.getItems().add(i);
        }


        dbConnection.disconnect();

    }

    @FXML
    public void buttonGetKlientOnAction() throws SQLException, ClassNotFoundException {

        updateTableViewKlient();

    }

    @FXML
    public void buttonAddKlientOnAction(){

        NewClientDialog newClientDialog = new NewClientDialog();
        Klient klient = newClientDialog.newClient();
        if(klient != null) {
            DBConnection dbConnection= new DBConnection();
            dbConnection.connect();
            try {
                klient.insertIntoDB(dbConnection);
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            try {
                updateTableViewKlient();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            dbConnection.disconnect();
        }

    }

    @FXML
    public void buttonEditKlientOnAction(){

        int index = tableViewKlient.getSelectionModel().getFocusedIndex();
        Klient klient =(Klient) tableViewKlient.getItems().get(index);
        NewClientDialog newClientDialog = new NewClientDialog();
        klient = newClientDialog.editClient(klient);
        if(klient != null) {
            DBConnection dbConnection= new DBConnection();
            dbConnection.connect();
            try {
                klient.updateDB(dbConnection);
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            dbConnection.disconnect();
            try {
                updateTableViewKlient();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

    }

    @FXML
    public void buttonDeleteKlientOnAction(){

        if(DBConnection.getLoggedUser().equals("pracownik")) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Błąd");
            alert.setHeaderText("Brak uprawnień");
            alert.setContentText("Ta operacja może zostać wykonana jedynie z poziomu konta \"admin\" ");

            alert.showAndWait();
            return;
        }

        int index = tableViewKlient.getSelectionModel().getFocusedIndex();
        Klient klient =(Klient) tableViewKlient.getItems().get(index);

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Ostrzeżenie");
        alert.setHeaderText("Próbujesz usunąć klienta, \nrazem z nim zostaną usunięte informacje o jego pojazdach i naprawach");
        alert.setContentText("Czy na pewno chcesz usunąć tego klienta?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK && klient != null){
            DBConnection dbConnection= new DBConnection();
            dbConnection.connect();
            try {
                klient.deleteFromDB(dbConnection);
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            dbConnection.disconnect();
            try {
                updateTableViewKlient();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    @FXML
    public void buttonSearchKlientOnAction() throws SQLException {
        updateTableViewSearchKlient();
    }

    @FXML
    public void buttonShowPojazdFromKlientOnAction() throws SQLException {
        updateTableViewPojazdFromKlient();
        tabPane.getSelectionModel().select(tabPojazd);
    }

    @FXML
    public void buttonShowNaprawaFromKlientOnAction() throws SQLException {
        updateTableViewNaprawaFromKlient();
        tabPane.getSelectionModel().select(tabNaprawa);
    }

    /**PRACOWNIK*/

    private void updateTableViewPracownik() throws SQLException, ClassNotFoundException {
        DBConnection dbConnection= new DBConnection();

        dbConnection.connect();

        ArrayList<Pracownik> listaP = Pracownik.getList(dbConnection,0);
        tableViewPracownik.getItems().removeAll(tableViewPracownik.getItems());
        for (Pracownik i:listaP) {
            tableViewPracownik.getItems().add(i);
        }

        dbConnection.disconnect();
    }

    private void updateTableViewNaprawaFromPracownik() throws SQLException{
        DBConnection dbConnection= new DBConnection();

        dbConnection.connect();

        int id = tableViewPracownik.getSelectionModel().getFocusedIndex();
        Pracownik pracownik = (Pracownik) tableViewPracownik.getItems().get(id);
        ArrayList<NaprawaUproszczony> listaN = Pracownik.getNaprawy(dbConnection,pracownik.getPESEL(),0);
        tableViewNaprawa.getItems().removeAll(tableViewNaprawa.getItems());
        for (NaprawaUproszczony i:listaN) {
            tableViewNaprawa.getItems().add(i);
        }


        dbConnection.disconnect();

    }

    private void updateTableViewSearchPracownik() throws SQLException{
        DBConnection dbConnection= new DBConnection();

        dbConnection.connect();

        ArrayList<Pracownik> listaK = Pracownik.searchFor(dbConnection,0,textFieldSearchPracownik.getText());
        tableViewPracownik.getItems().removeAll(tableViewPracownik.getItems());
        for (Pracownik i:listaK) {
            tableViewPracownik.getItems().add(i);
        }

        dbConnection.disconnect();
    }

    @FXML
    public void buttonGetPracownikOnAction() throws SQLException, ClassNotFoundException {

        updateTableViewPracownik();

    }

    @FXML
    public void buttonAddPracownikOnAction(){

        if(DBConnection.getLoggedUser().equals("pracownik")) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Błąd");
            alert.setHeaderText("Brak uprawnień");
            alert.setContentText("Ta operacja może zostać wykonana jedynie z poziomu konta \"admin\" ");

            alert.showAndWait();
            return;
        }

        NowyPracownikDialog nowyPracownikDialog = new NowyPracownikDialog();
        Pracownik pracownik = nowyPracownikDialog.newPracownik();
        if(pracownik != null) {
            DBConnection dbConnection= new DBConnection();
            dbConnection.connect();
            try {
                pracownik.insertIntoDB(dbConnection);
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            try {
                updateTableViewPracownik();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            dbConnection.disconnect();
        }

    }

    @FXML
    public void buttonEditPracownikOnAction(){

        if(DBConnection.getLoggedUser().equals("pracownik")) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Błąd");
            alert.setHeaderText("Brak uprawnień");
            alert.setContentText("Ta operacja może zostać wykonana jedynie z poziomu konta \"admin\" ");

            alert.showAndWait();
            return;
        }

        int index = tableViewPracownik.getSelectionModel().getFocusedIndex();
        Pracownik pracownik =(Pracownik) tableViewPracownik.getItems().get(index);
        NowyPracownikDialog nowyPracownikDialog = new NowyPracownikDialog();
        pracownik = nowyPracownikDialog.editPracownik(pracownik);
        if(pracownik != null) {
            DBConnection dbConnection= new DBConnection();
            dbConnection.connect();
            try {
                pracownik.updateDB(dbConnection);
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            dbConnection.disconnect();
            try {
                updateTableViewPracownik();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

    }

    @FXML
    public void buttonDeletePracownikOnAction(){

        if(DBConnection.getLoggedUser().equals("pracownik")) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Błąd");
            alert.setHeaderText("Brak uprawnień");
            alert.setContentText("Ta operacja może zostać wykonana jedynie z poziomu konta \"admin\" ");

            alert.showAndWait();
            return;
        }

        int index = tableViewPracownik.getSelectionModel().getFocusedIndex();
        Pracownik pracownik =(Pracownik) tableViewPracownik.getItems().get(index);

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Ostrzeżenie");
        alert.setHeaderText("Próbujesz usunąć Pracownika, \nrazem z nim zostaną usunięte informacje o jego naprawach");
        alert.setContentText("Czy na pewno chcesz usunąć tego pracownika?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK && pracownik != null){
            DBConnection dbConnection= new DBConnection();
            dbConnection.connect();
            try {
                pracownik.deleteFromDB(dbConnection);
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            dbConnection.disconnect();
            try {
                updateTableViewPracownik();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    @FXML
    public void buttonSearchPracownikOnAction() throws SQLException {
        updateTableViewSearchPracownik();
    }

    @FXML
    public void buttonShowNaprawyFromPracownikOnAction() throws SQLException {
        updateTableViewNaprawaFromPracownik();
        tabPane.getSelectionModel().select(tabNaprawa);
    }

    /**POJAZD*/


    private void updateTableViewPojazd() throws SQLException, ClassNotFoundException {
        DBConnection dbConnection= new DBConnection();

        dbConnection.connect();

        ArrayList<Pojazd> listaPoj = Pojazd.getList(dbConnection,0);
        tableViewPojazd.getItems().removeAll(tableViewPojazd.getItems());
        for (Pojazd i:listaPoj) {
            tableViewPojazd.getItems().add(i);
        }

        dbConnection.disconnect();
    }

    private void updateTableViewNaprawaFromPojazd() throws SQLException{
        DBConnection dbConnection= new DBConnection();

        dbConnection.connect();

        int id = tableViewPojazd.getSelectionModel().getFocusedIndex();
        Pojazd pojazd = (Pojazd) tableViewPojazd.getItems().get(id);
        ArrayList<NaprawaUproszczony> listaN = Pojazd.getNaprawy(dbConnection,pojazd.getVIN(),0);
        tableViewNaprawa.getItems().removeAll(tableViewNaprawa.getItems());
        for (NaprawaUproszczony i:listaN) {
            tableViewNaprawa.getItems().add(i);
        }


        dbConnection.disconnect();

    }

    private void updateTableViewKlientFromPojazd() throws SQLException{
        DBConnection dbConnection= new DBConnection();

        dbConnection.connect();

        int id = tableViewPojazd.getSelectionModel().getFocusedIndex();
        Pojazd pojazd = (Pojazd) tableViewPojazd.getItems().get(id);
        Klient klient = Pojazd.getKlient(dbConnection,pojazd.getVIN());
        tableViewKlient.getItems().removeAll(tableViewKlient.getItems());
        tableViewKlient.getItems().add(klient);



        dbConnection.disconnect();

    }

    private void updateTableViewSearchPojazd() throws SQLException{
        DBConnection dbConnection= new DBConnection();

        dbConnection.connect();

        ArrayList<Pojazd> listaPoj = Pojazd.searchFor(dbConnection,0,textFieldSearchPojazd.getText());
        tableViewPojazd.getItems().removeAll(tableViewPojazd.getItems());
        for (Pojazd i:listaPoj) {
            tableViewPojazd.getItems().add(i);
        }

        dbConnection.disconnect();
    }

    @FXML
    public void buttonGetPojazdOnAction() throws SQLException, ClassNotFoundException {

        updateTableViewPojazd();

    }

    @FXML
    public void buttonAddPojazdOnAction(){

        NowyPojazdDialog nowyPojazdDialog = new NowyPojazdDialog();
        Pojazd pojazd = nowyPojazdDialog.newPojazd();
        if(pojazd != null) {
            DBConnection dbConnection= new DBConnection();
            dbConnection.connect();
            try {
                pojazd.insertIntoDB(dbConnection);
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            try {
                updateTableViewPojazd();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            dbConnection.disconnect();
        }

    }

    @FXML
    public void buttonEditPojazdOnAction(){

        int index = tableViewPojazd.getSelectionModel().getFocusedIndex();
        Pojazd pojazd =(Pojazd) tableViewPojazd.getItems().get(index);
        NowyPojazdDialog nowyPojazdDialog = new NowyPojazdDialog();
        pojazd = nowyPojazdDialog.editPojazd(pojazd);
        if(pojazd != null) {
            DBConnection dbConnection= new DBConnection();
            dbConnection.connect();
            try {
                pojazd.updateDB(dbConnection);
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            dbConnection.disconnect();
            try {
                updateTableViewPojazd();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

    }

    @FXML
    public void buttonDeletePojazdOnAction(){

        if(DBConnection.getLoggedUser().equals("pracownik")) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Błąd");
            alert.setHeaderText("Brak uprawnień");
            alert.setContentText("Ta operacja może zostać wykonana jedynie z poziomu konta \"admin\" ");

            alert.showAndWait();
            return;
        }

        int index = tableViewPojazd.getSelectionModel().getFocusedIndex();
        Pojazd pojazd =(Pojazd) tableViewPojazd.getItems().get(index);

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Ostrzeżenie");
        alert.setHeaderText("Próbujesz usunąć Pojazd, \nrazem z nim zostaną usunięte informacje o jego naprawach");
        alert.setContentText("Czy na pewno chcesz usunąć ten pojazd?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK && pojazd != null){
            DBConnection dbConnection= new DBConnection();
            dbConnection.connect();
            try {
                pojazd.deleteFromDB(dbConnection);
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            dbConnection.disconnect();
            try {
                updateTableViewPojazd();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    @FXML
    public void buttonSearchPojazdOnAction() throws SQLException {
        updateTableViewSearchPojazd();
    }


    @FXML
    public void buttonShowNaprawaPojazdOnAction() throws SQLException {
        updateTableViewNaprawaFromPojazd();
        tabPane.getSelectionModel().select(tabNaprawa);
    }
    @FXML
    public void buttonShowKlientPojazdOnAction() throws SQLException {
        updateTableViewKlientFromPojazd();
        tabPane.getSelectionModel().select(tabKlient);
    }

    /**NAPRAWA*/

    private void updateTableViewNaprawa() throws SQLException, ClassNotFoundException {
        DBConnection dbConnection= new DBConnection();

        dbConnection.connect();

        ArrayList<NaprawaUproszczony> listaN = NaprawaUproszczony.getList(dbConnection,0);
        tableViewNaprawa.getItems().removeAll(tableViewNaprawa.getItems());
        for (NaprawaUproszczony i:listaN) {
            tableViewNaprawa.getItems().add(i);
        }

        dbConnection.disconnect();
    }

    private void updateTableViewSearchNaprawa() throws SQLException{
        DBConnection dbConnection= new DBConnection();

        dbConnection.connect();

        ArrayList<NaprawaUproszczony> listaN = Naprawa.searchFor(dbConnection,datePickerFromNaprawa.getValue(),datePickerToNaprawa.getValue(),0);

        tableViewNaprawa.getItems().removeAll(tableViewNaprawa.getItems());
        for (NaprawaUproszczony i:listaN) {
            tableViewNaprawa.getItems().add(i);
        }

        dbConnection.disconnect();
    }


    private void updateTableViewPojazdFromNaprawa() throws SQLException{
        DBConnection dbConnection= new DBConnection();

        dbConnection.connect();

        int id = tableViewNaprawa.getSelectionModel().getFocusedIndex();
        NaprawaUproszczony naprawaUproszczony = (NaprawaUproszczony) tableViewNaprawa.getItems().get(id);
        Pojazd pojazd = Naprawa.getPojazd(dbConnection,naprawaUproszczony.getIdNaprawa());
        tableViewPojazd.getItems().removeAll(tableViewPojazd.getItems());
        tableViewPojazd.getItems().add(pojazd);


        dbConnection.disconnect();

    }

    private void updateTableViewPracownikFromNaprawa() throws SQLException{
        DBConnection dbConnection= new DBConnection();

        dbConnection.connect();

        int id = tableViewNaprawa.getSelectionModel().getFocusedIndex();
        NaprawaUproszczony naprawaUproszczony = (NaprawaUproszczony) tableViewNaprawa.getItems().get(id);
        Pracownik pracownik = Naprawa.getPracownik(dbConnection,naprawaUproszczony.getIdNaprawa());
        tableViewPracownik.getItems().removeAll(tableViewPracownik.getItems());
        tableViewPracownik.getItems().add(pracownik);


        dbConnection.disconnect();

    }

    private void updateTableViewKlientFromNaprawa() throws SQLException{
        DBConnection dbConnection= new DBConnection();

        dbConnection.connect();

        int id = tableViewNaprawa.getSelectionModel().getFocusedIndex();
        NaprawaUproszczony naprawaUproszczony = (NaprawaUproszczony) tableViewNaprawa.getItems().get(id);
        Klient klient = Naprawa.getKlient(dbConnection,naprawaUproszczony.getIdNaprawa());
        tableViewKlient.getItems().removeAll(tableViewKlient.getItems());
        tableViewKlient.getItems().add(klient);


        dbConnection.disconnect();

    }


    @FXML
    public void buttonGetNaprawaOnAction() throws SQLException, ClassNotFoundException {

        updateTableViewNaprawa();

    }

    @FXML
    public void buttonAddNaprawaOnAction(){

        NowaNaprawaDialog nowaNaprawaDialog = new NowaNaprawaDialog();
        Naprawa naprawa = nowaNaprawaDialog.newNaprawa();
        if(naprawa != null) {
            DBConnection dbConnection= new DBConnection();
            dbConnection.connect();
            try {
                naprawa.insertIntoDB(dbConnection);
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            try {
                updateTableViewNaprawa();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            dbConnection.disconnect();
        }

    }

    @FXML
    public void buttonEditNaprawaOnAction() throws SQLException {

        int index = tableViewNaprawa.getSelectionModel().getFocusedIndex();
        NaprawaUproszczony naprawaUproszczony =(NaprawaUproszczony) tableViewNaprawa.getItems().get(index);

        Naprawa naprawa = null;

        DBConnection dbConnection= new DBConnection();
        dbConnection.connect();
        naprawa = naprawaUproszczony.getNaprawaFromDB(dbConnection);
        dbConnection.disconnect();

        NowaNaprawaDialog nowaNaprawaDialog = new NowaNaprawaDialog();
        naprawa = nowaNaprawaDialog.editNaprawa(naprawa);
        if(naprawa != null) {

            dbConnection.connect();
            try {
                naprawa.updateDB(dbConnection);
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            dbConnection.disconnect();
            try {
                updateTableViewNaprawa();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    @FXML
    public void buttonDeleteNaprawaOnAction(){

        if(DBConnection.getLoggedUser().equals("pracownik")) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Błąd");
            alert.setHeaderText("Brak uprawnień");
            alert.setContentText("Ta operacja może zostać wykonana jedynie z poziomu konta \"admin\" ");

            alert.showAndWait();
            return;
        }

        int index = tableViewNaprawa.getSelectionModel().getFocusedIndex();
        NaprawaUproszczony naprawaUproszczony = (NaprawaUproszczony) tableViewNaprawa.getItems().get(index);

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Ostrzeżenie");
        alert.setHeaderText("Próbujesz usunąć Naprawa, \nrazem z nim zostaną usunięte informacje o jego naprawach");
        alert.setContentText("Czy na pewno chcesz usunąć ten naprawa?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK && naprawaUproszczony != null){
            DBConnection dbConnection= new DBConnection();
            dbConnection.connect();
            try {
                naprawaUproszczony.getNaprawaFromDB(dbConnection).deleteFromDB(dbConnection);
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            dbConnection.disconnect();
            try {
                updateTableViewNaprawa();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                    e.printStackTrace();
            }
        }
    }

    @FXML
    public void buttonSearchNaprawaOnAction() throws SQLException {
        updateTableViewSearchNaprawa();
    }
    @FXML
    public void buttonCleardatePickerFromNaprawaOnAction() {
        datePickerFromNaprawa.setValue(null);

    }
    @FXML
    public void buttonCleardatePickerToNaprawaOnAction() {
        datePickerToNaprawa.setValue(null);

    }

    @FXML
    public void buttonShowPojazdNaprawaOnAction() throws SQLException {
        updateTableViewPojazdFromNaprawa();
        tabPane.getSelectionModel().select(tabPojazd);
    }
    @FXML
    public void buttonShowPracownikNaprawaOnAction() throws SQLException {
        updateTableViewPracownikFromNaprawa();
        tabPane.getSelectionModel().select(tabPracownik);
    }
    @FXML
    public void buttonShowKlientNaprawaOnAction() throws SQLException {
        updateTableViewKlientFromNaprawa();
        tabPane.getSelectionModel().select(tabKlient);
    }

    @FXML
    public void buttonShowNaprawaSzczegolyOnAction() throws SQLException {

        DBConnection dbConnection= new DBConnection();

        dbConnection.connect();

        int id = tableViewNaprawa.getSelectionModel().getFocusedIndex();
        NaprawaUproszczony naprawaUproszczony = (NaprawaUproszczony) tableViewNaprawa.getItems().get(id);
        String info = NaprawaSzczegoly.getNaprawaSzczegoly(dbConnection,naprawaUproszczony.getIdNaprawa()).toString();

        dbConnection.disconnect();

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Info");
        alert.setHeaderText("Szczegółowe informacje o naprawie: #"+Integer.toString(naprawaUproszczony.getIdNaprawa()));

        TextArea textArea = new TextArea(info);
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(textArea, 0, 1);

// Set expandable Exception into the dialog pane.
        alert.getDialogPane().setExpandableContent(expContent);
        alert.getDialogPane().setExpanded(true);

        alert.showAndWait();

    }


}


