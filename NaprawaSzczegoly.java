package sample;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

public class NaprawaSzczegoly {

    private int idNaprawa;
    private String Imie_Klient;
    private String Nazwisko_Klient;
    private String Rejestracja;
    private String Marka;
    private String Model;
    private Date Data_przyjecia;
    private Date Data_oddania;
    private String Zgloszone_usterki;
    private String Przeprowadzone_naprawy;
    private Float Koszt_napraw;
    private Float Koszt_czesci;
    private int Paliwo_przed;
    private Integer Paliwo_po;
    private String Imie_Pracownik;
    private String Nazwisko_Pracownik;

    public NaprawaSzczegoly(int idNaprawa, String imie_Klient, String nazwisko_Klient, String rejestracja, String marka, String model,
                            Date data_przyjecia, Date data_oddania, String zgloszone_usterki, String przeprowadzone_naprawy,
                            Float koszt_napraw, Float koszt_czesci, int paliwo_przed, Integer paliwo_po, String imie_Pracownik,
                            String nazwisko_Pracownik) {
        this.idNaprawa = idNaprawa;
        Imie_Klient = imie_Klient;
        Nazwisko_Klient = nazwisko_Klient;
        Rejestracja = rejestracja;
        Marka = marka;
        Model = model;
        Data_przyjecia = data_przyjecia;
        Data_oddania = data_oddania;
        Zgloszone_usterki = zgloszone_usterki;
        Przeprowadzone_naprawy = przeprowadzone_naprawy;
        Koszt_napraw = koszt_napraw;
        Koszt_czesci = koszt_czesci;
        Paliwo_przed = paliwo_przed;
        Paliwo_po = paliwo_po;
        Imie_Pracownik = imie_Pracownik;
        Nazwisko_Pracownik = nazwisko_Pracownik;
    }

    public static NaprawaSzczegoly getNaprawaSzczegoly(DBConnection dbConnection, int idNaprawa) throws SQLException {
        NaprawaSzczegoly naprawaSzczegoly = null;

        String command = "SELECT * FROM warsztat.naprawaszczegoly WHERE ID = " + idNaprawa;
        ResultSet rs = dbConnection.getDataFromDB(command);
        try {

            while (rs.next()) {
                naprawaSzczegoly = new NaprawaSzczegoly(rs.getInt("ID"),rs.getString("Imie_Klient"),
                        rs.getString("Nazwisko_Klient"),rs.getString("Rejestracja"),
                        rs.getString("Marka"),rs.getString("Model"),
                        rs.getDate("Data_przyjecia"),rs.getDate("Data_oddania"),
                        rs.getString("Zgloszone_usterki"),rs.getString("Przeprowadzone_naprawy"),
                        DBConnection.getFloatFromDB(rs,"Koszt_napraw"),DBConnection.getFloatFromDB(rs,"Koszt_czesci"),
                        rs.getInt("Paliwo_przed"),DBConnection.getIntegerFromDB(rs,"Paliwo_po"),
                        rs.getString("Imie_Pracownik"),rs.getString("Nazwisko_Pracownik"));
            }

        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }

        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException sqlEx) {
            }
        }

        return naprawaSzczegoly;
    }

    @Override
    public String toString() {
        String text = null;
        try {
            text = "ID: "+idNaprawa+"\n"+
                    "Imię i nazwisko klienta: "+Imie_Klient+" "+Nazwisko_Klient+"\n\n"+
                    "Numer rejestracyjny: "+Rejestracja+"\n"+
                    "Marka: " +Marka+"\n"+
                    "Model: " +Model+"\n\n"+
                    "Data przyjęcia: " +Data_przyjecia.toString()+"\n"+
                    "Data oddania: " +DBConnection.DateToString(Data_oddania)+"\n\n"+
                    "Zgłoszone usterki: " +Zgloszone_usterki+"\n"+
                    "Przeprowadzone naprawy: " +Przeprowadzone_naprawy+"\n\n"+
                    "Robocizna: " +DBConnection.FloatToString(Koszt_napraw)+"\n"+
                    "Koszt części: " +DBConnection.FloatToString(Koszt_czesci)+"\n\n"+
                    "Stan paliwa przed naprawą: " +Integer.toString(Paliwo_przed)+"\n"+
                    "Stan paliwa po naprawie: " +DBConnection.IntegerToString(Paliwo_po)+"\n\n"+
                    "Imię i nazwisko pracownika: "+Imie_Pracownik+" "+Nazwisko_Pracownik;
        } catch (SQLException e) {
            e.printStackTrace();
        }


        return text;
    }


    public int getIdNaprawa() {
        return idNaprawa;
    }

    public void setIdNaprawa(int idNaprawa) {
        this.idNaprawa = idNaprawa;
    }

    public String getImie_Klient() {
        return Imie_Klient;
    }

    public void setImie_Klient(String imie_Klient) {
        Imie_Klient = imie_Klient;
    }

    public String getNazwisko_Klient() {
        return Nazwisko_Klient;
    }

    public void setNazwisko_Klient(String nazwisko_Klient) {
        Nazwisko_Klient = nazwisko_Klient;
    }

    public String getRejestracja() {
        return Rejestracja;
    }

    public void setRejestracja(String rejestracja) {
        Rejestracja = rejestracja;
    }

    public String getMarka() {
        return Marka;
    }

    public void setMarka(String marka) {
        Marka = marka;
    }

    public String getModel() {
        return Model;
    }

    public void setModel(String model) {
        Model = model;
    }

    public Date getData_przyjecia() {
        return Data_przyjecia;
    }

    public void setData_przyjecia(Date data_przyjecia) {
        Data_przyjecia = data_przyjecia;
    }

    public Date getData_oddania() {
        return Data_oddania;
    }

    public void setData_oddania(Date data_oddania) {
        Data_oddania = data_oddania;
    }

    public String getZgloszone_usterki() {
        return Zgloszone_usterki;
    }

    public void setZgloszone_usterki(String zgloszone_usterki) {
        Zgloszone_usterki = zgloszone_usterki;
    }

    public String getPrzeprowadzone_naprawy() {
        return Przeprowadzone_naprawy;
    }

    public void setPrzeprowadzone_naprawy(String przeprowadzone_naprawy) {
        Przeprowadzone_naprawy = przeprowadzone_naprawy;
    }

    public Float getKoszt_napraw() {
        return Koszt_napraw;
    }

    public void setKoszt_napraw(Float koszt_napraw) {
        Koszt_napraw = koszt_napraw;
    }

    public Float getKoszt_czesci() {
        return Koszt_czesci;
    }

    public void setKoszt_czesci(Float koszt_czesci) {
        Koszt_czesci = koszt_czesci;
    }

    public int getPaliwo_przed() {
        return Paliwo_przed;
    }

    public void setPaliwo_przed(int paliwo_przed) {
        Paliwo_przed = paliwo_przed;
    }

    public int getPaliwo_po() {
        return Paliwo_po;
    }

    public void setPaliwo_po(int paliwo_po) {
        Paliwo_po = paliwo_po;
    }

    public String getImie_Pracownik() {
        return Imie_Pracownik;
    }

    public void setImie_Pracownik(String imie_Pracownik) {
        Imie_Pracownik = imie_Pracownik;
    }

    public String getNazwisko_Pracownik() {
        return Nazwisko_Pracownik;
    }

    public void setNazwisko_Pracownik(String nazwisko_Pracownik) {
        Nazwisko_Pracownik = nazwisko_Pracownik;
    }
}
