package sample;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Pojazd{

    private String VIN;
    private String Rejestracja;
    private int Rok;
    private String Marka;
    private String Model;
    private String Klient_PESEL;
    private String oldVIN;


    public Pojazd(String VIN, String rejestracja, int rok, String marka, String model, String klient_PESEL) {
        this.VIN = VIN;
        Rejestracja = rejestracja;
        Rok = rok;
        Marka = marka;
        Model = model;
        Klient_PESEL = klient_PESEL;
    }

    public Pojazd() {

    }

    public void insertIntoDB(DBConnection dbConnection) throws SQLException, ClassNotFoundException {
        String command = "INSERT INTO `warsztat`.`pojazd` (`VIN`, `Rejestracja`, `Rok`, `Marka`, `Model`, `Klient_PESEL`) VALUES ('"
                +VIN+"', '"+Rejestracja+"', "+Rok+", '"+Marka+"', '"+Model+"', '"+Klient_PESEL+"')";
        dbConnection.execute(command);
    }

    public void updateDB(DBConnection dbConnection) throws SQLException, ClassNotFoundException {
        String command = "UPDATE `warsztat`.`pojazd`  SET VIN = '"+VIN+"', Rejestracja = '"+Rejestracja+"', Rok = '"+Integer.toString(Rok)+
                "', Marka = '"+Marka+"', Model = '"+Model+"', Klient_PESEL = '"+Klient_PESEL+"' WHERE VIN = '"+oldVIN+"'";
        dbConnection.execute(command);
    }

    public void deleteFromDB(DBConnection dbConnection) throws SQLException, ClassNotFoundException {
        String command = "DELETE FROM warsztat.pojazd WHERE VIN = '"+VIN+"'";
        dbConnection.execute(command);
    }

    public static ArrayList<Pojazd> getList(DBConnection dbConnection, int limit) throws SQLException, ClassNotFoundException {
        ArrayList<Pojazd> lista = new ArrayList<>();

        String command = "SELECT * FROM warsztat.pojazd";
        if(limit>0) command+=" LIMIT "+limit;
        ResultSet rs = dbConnection.getDataFromDB(command);

        try {

            while (rs.next()) {
                lista.add(new Pojazd(rs.getString("VIN"),rs.getString("Rejestracja"),
                        Integer.parseInt(rs.getString("Rok")),
                        rs.getString("Marka"),rs.getString("Model"),rs.getString("Klient_PESEL")));
            }

        }
        catch (SQLException ex){
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }

        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException sqlEx) {
            }
        }

        return lista;
    }

    public static ArrayList<NaprawaUproszczony> getNaprawy(DBConnection dbConnection, String VIN ,int limit) throws SQLException {
        ArrayList<NaprawaUproszczony> lista = new ArrayList<>();

        String command = "SELECT warsztat.naprawauproszczony.* " +
                "FROM " +
                "warsztat.naprawauproszczony, warsztat.naprawa, warsztat.pojazd " +
                "WHERE " +
                "warsztat.naprawauproszczony.ID = warsztat.naprawa.idNaprawa " +
                "AND warsztat.naprawa.Pojazd_VIN = warsztat.pojazd.VIN " +
                "AND VIN = '"+VIN+"'";

        if(limit>0) command+=" LIMIT "+limit;
        ResultSet rs = dbConnection.getDataFromDB(command);

        try {

            while (rs.next()) {
                lista.add(new NaprawaUproszczony(Integer.parseInt(rs.getString("ID")),
                        rs.getString("Imie"),rs.getString("Nazwisko"),rs.getString("Rejestracja"),
                        rs.getString("Marka"),rs.getString("Model"),
                        Date.valueOf(rs.getString("Data_przyjecia")),Date.valueOf(rs.getString("Data_oddania"))));
            }

        }
        catch (SQLException ex){
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }

        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException sqlEx) {
            }
        }

        return lista;
    }

    public static Klient getKlient(DBConnection dbConnection, String VIN) throws SQLException {
        Klient klient = new Klient();

        String command = "SELECT warsztat.klient.* " +
                "FROM " +
                "warsztat.klient, warsztat.pojazd " +
                "WHERE " +
                "warsztat.klient.PESEL = warsztat.pojazd.Klient_PESEL " +
                "        AND warsztat.pojazd.VIN = '"+VIN+"'";

        ResultSet rs = dbConnection.getDataFromDB(command);

        try {
            while (rs.next()) {
                klient = new Klient(rs.getString("PESEL"),rs.getString("Imie"),
                        rs.getString("Nazwisko"),Integer.parseInt(rs.getString("Telefon")),
                        rs.getString("Email"),rs.getString("NIP"));
            }

        }
        catch (SQLException ex){
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }

        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException sqlEx) {
            }
        }

        return klient;
    }

    public static ArrayList<Pojazd> searchFor(DBConnection dbConnection, int limit,String text) throws SQLException {
        ArrayList<Pojazd> lista = new ArrayList<>();

        String[] tmp = text.split(" ");

        String command = "";
        if(text.isEmpty()) command = "SELECT * FROM warsztat.pojazd";
        else if(tmp.length == 1) command = "SELECT * FROM warsztat.pojazd WHERE Marka LIKE '"+tmp[0]+"%' OR Model LIKE '"+tmp[0]+"%'";
        else if(tmp.length == 2) command = "SELECT * FROM warsztat.pojazd WHERE Marka LIKE '"+tmp[0]+"' AND Model LIKE '"+tmp[1]+"%'";

        if(limit>0) command+=" LIMIT "+limit;
        ResultSet rs = dbConnection.getDataFromDB(command);
        try {

            while (rs.next()) {
                lista.add(new Pojazd(rs.getString("VIN"),rs.getString("Rejestracja"),Integer.parseInt(rs.getString("Rok")),
                        rs.getString("Marka"),rs.getString("Model"),rs.getString("Klient_PESEL")));
            }

        }
        catch (SQLException ex){
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }

        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException sqlEx) {
            }
        }
        return lista;
    }

    @Override
    public String toString() {
        return "['"+VIN+"', '"+Rejestracja+"', "+Rok+", '"+Marka+"', '"+Model+"', '"+Klient_PESEL+"'];";
    }

    public String getVIN() {
        return VIN;
    }

    public void setVIN(String VIN) {
        this.VIN = VIN;
    }

    public String getRejestracja() {
        return Rejestracja;
    }

    public void setRejestracja(String rejestracja) {
        Rejestracja = rejestracja;
    }

    public int getRok() {
        return Rok;
    }

    public void setRok(int rok) {
        Rok = rok;
    }

    public String getMarka() {
        return Marka;
    }

    public void setMarka(String marka) {
        Marka = marka;
    }

    public String getModel() {
        return Model;
    }

    public void setModel(String model) {
        Model = model;
    }

    public String getKlient_PESEL() {
        return Klient_PESEL;
    }

    public void setKlient_PESEL(String klient_PESEL) {
        Klient_PESEL = klient_PESEL;
    }

    public String getOldVIN() {
        return oldVIN;
    }

    public void setOldVIN(String oldVIN) {
        this.oldVIN = oldVIN;
    }
}
