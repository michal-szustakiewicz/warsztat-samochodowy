package sample;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Klient{

    private String PESEL;
    private String Imie;
    private String Nazwisko;
    private int Telefon;
    private String Email;
    private String NIP;
    private String oldPESEL;

    public Klient() {
    }

    public Klient(String PESEL, String imie, String nazwisko, int telefon, String email, String NIP) {
        this.PESEL = PESEL;
        Imie = imie;
        Nazwisko = nazwisko;
        Telefon = telefon;
        Email = email;
        this.NIP = NIP;
    }

    public void insertIntoDB(DBConnection dbConnection) throws SQLException, ClassNotFoundException {
        String command = "INSERT INTO `warsztat`.`Klient` (`PESEL`, `Imie`, `Nazwisko`, `Telefon`, `Email`, `NIP`) VALUES ('"
                + PESEL+"' ,'"+ Imie+"' ,'"+ Nazwisko+"' ,"+ Telefon+" ,"+ Email+" ,"+ NIP+")";
        dbConnection.execute(command);
    }

    public void updateDB(DBConnection dbConnection) throws SQLException, ClassNotFoundException {
        String command = "UPDATE warsztat.klient SET PESEL = '"+PESEL+"', Imie = '"+Imie+"', Nazwisko = '"+Nazwisko+"', Telefon = "+Telefon+", Email = "+Email+", NIP = "+NIP+" WHERE PESEL = "+oldPESEL;
        dbConnection.execute(command);
    }

    public void deleteFromDB(DBConnection dbConnection) throws SQLException, ClassNotFoundException {
        String command = "DELETE FROM warsztat.klient WHERE PESEL = "+PESEL;
        dbConnection.execute(command);
    }

    public static ArrayList<Klient> getList(DBConnection dbConnection, int limit) throws SQLException, ClassNotFoundException {
        ArrayList<Klient> lista = new ArrayList<>();

        String command = "SELECT * FROM warsztat.klient";
        if(limit>0) command+=" LIMIT "+limit;
        ResultSet rs = dbConnection.getDataFromDB(command);
        try {

            while (rs.next()) {
                lista.add(new Klient(rs.getString("PESEL"),rs.getString("Imie"),
                        rs.getString("Nazwisko"),Integer.parseInt(rs.getString("Telefon")),
                        rs.getString("Email"),rs.getString("NIP")));
            }

        }
        catch (SQLException ex){
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }

        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException sqlEx) {
            }
        }

        return lista;
    }

    public static ArrayList<NaprawaUproszczony> getNaprawy(DBConnection dbConnection, String PESEL ,int limit) throws SQLException {
        ArrayList<NaprawaUproszczony> lista = new ArrayList<>();

        String command = "SELECT warsztat.naprawauproszczony.* " +
                "FROM " +
                "warsztat.naprawauproszczony, warsztat.naprawa, warsztat.pojazd, warsztat.klient " +
                "WHERE " +
                "warsztat.naprawauproszczony.ID = warsztat.naprawa.idNaprawa " +
                "AND warsztat.naprawa.Pojazd_VIN = warsztat.pojazd.VIN " +
                "AND warsztat.klient.PESEL = warsztat.pojazd.Klient_PESEL " +
                "AND warsztat.klient.PESEL = '"+PESEL+"'";

        if(limit>0) command+=" LIMIT "+limit;
        ResultSet rs = dbConnection.getDataFromDB(command);

        try {

            while (rs.next()) {
                lista.add(new NaprawaUproszczony(Integer.parseInt(rs.getString("ID")),
                        rs.getString("Imie"),rs.getString("Nazwisko"),rs.getString("Rejestracja"),
                        rs.getString("Marka"),rs.getString("Model"),
                        Date.valueOf(rs.getString("Data_przyjecia")),Date.valueOf(rs.getString("Data_oddania"))));
            }

        }
        catch (SQLException ex){
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }

        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException sqlEx) {
            }
        }

        return lista;
    }

    public static ArrayList<Pojazd> getPojazdy(DBConnection dbConnection, String PESEL ,int limit) throws SQLException {
        ArrayList<Pojazd> lista = new ArrayList<>();

        String command = "SELECT warsztat.pojazd.* " +
                "FROM " +
                "warsztat.pojazd, warsztat.klient " +
                "WHERE " +
                "warsztat.klient.PESEL = warsztat.pojazd.Klient_PESEL " +
                "AND warsztat.klient.PESEL = '"+PESEL+"'";

        if(limit>0) command+=" LIMIT "+limit;
        ResultSet rs = dbConnection.getDataFromDB(command);

        try {

            while (rs.next()) {
                lista.add(new Pojazd(rs.getString("VIN"),rs.getString("Rejestracja"),
                        Integer.parseInt(rs.getString("Rok")),
                        rs.getString("Marka"),rs.getString("Model"),rs.getString("Klient_PESEL")));
            }

        }
        catch (SQLException ex){
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }

        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException sqlEx) {
            }
        }

        return lista;
    }

    public static ArrayList<Klient> searchFor(DBConnection dbConnection, int limit,String text) throws SQLException {
        ArrayList<Klient> lista = new ArrayList<>();

        String[] tmp = text.split(" ");

        String command = "";
        if(text.isEmpty()) command = "SELECT * FROM warsztat.klient";
        else if(tmp.length == 1) command = "SELECT * FROM warsztat.klient WHERE Imie LIKE '"+tmp[0]+"%' OR Nazwisko LIKE '"+tmp[0]+"%'";
        else if(tmp.length == 2) command = "SELECT * FROM warsztat.klient WHERE Imie LIKE '"+tmp[0]+"' AND Nazwisko LIKE '"+tmp[1]+"%'";

        if(limit>0) command+=" LIMIT "+limit;
        ResultSet rs = dbConnection.getDataFromDB(command);
        try {

            while (rs.next()) {
                lista.add(new Klient(rs.getString("PESEL"),rs.getString("Imie"),
                        rs.getString("Nazwisko"),Integer.parseInt(rs.getString("Telefon")),
                        rs.getString("Email"),rs.getString("NIP")));
            }

        }
        catch (SQLException ex){
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }

        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException sqlEx) {
            }
        }
        return lista;
    }

    @Override
    public String toString() {
        return "['"+ PESEL+"' ,'"+ Imie+"' ,'"+ Nazwisko+"' ,"+ Telefon+" ,'"+ Email+"' ,'"+ NIP+"'];";
    }

    public String getPESEL() {
        return PESEL;
    }

    public void setPESEL(String PESEL) {
        this.PESEL = PESEL;
    }

    public String getImie() {
        return Imie;
    }

    public void setImie(String imie) {
        Imie = imie;
    }

    public String getNazwisko() {
        return Nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        Nazwisko = nazwisko;
    }

    public int getTelefon() {
        return Telefon;
    }

    public void setTelefon(int telefon) {
        Telefon = telefon;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getNIP() {
        return NIP;
    }

    public void setNIP(String NIP) {
        this.NIP = NIP;
    }

    public String getOldPESEL() {
        return oldPESEL;
    }

    public void setOldPESEL(String oldPESEL) {
        this.oldPESEL = oldPESEL;
    }
}
