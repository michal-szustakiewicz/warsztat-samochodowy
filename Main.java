package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import sample.dialog.LoginDialog;

public class Main extends Application {

    static Stage primaryStage = null;

    @Override
    public void start(Stage primaryStage) throws Exception {

        LoginDialog loginDialog = new LoginDialog();
        if (!loginDialog.getLoginResult()) return;
        String loginName = DBConnection.getLoggedUser();
        Parent root = FXMLLoader.load(getClass().getResource("mainwindow.fxml"));
        primaryStage.setTitle("Warsztat, zalogowano jako: " + loginName);
        primaryStage.setScene(new Scene(root, 800, 600));
        primaryStage.show();
        this.primaryStage = primaryStage;

    }

    public static Stage getPrimaryStage() {
        return primaryStage;
    }

    public static void main(String[] args) {
        launch(args);
    }


}

