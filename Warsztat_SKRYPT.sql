CREATE DATABASE warsztat;
USE warsztat;
SET @@global.time_zone = '+01:00';


-- -----------------------------------------------------
-- Tabela: Klient
-- -----------------------------------------------------
CREATE TABLE warsztat.Klient (

  PESEL CHAR(11) NOT NULL,
  Imie VARCHAR(20) NOT NULL,
  Nazwisko VARCHAR(40) NOT NULL,
  Telefon INT NOT NULL,
  Email VARCHAR(30) NULL,
  NIP VARCHAR(10) NULL,
  PRIMARY KEY (PESEL));

CREATE UNIQUE INDEX PESEL_UNIQUE_INDEX ON warsztat.Klient (PESEL ASC);
CREATE INDEX IMIE_NAZWISKO_INDEX ON warsztat.Klient (imie, nazwisko);

-- -----------------------------------------------------
-- Tabela: Pojazd
-- -----------------------------------------------------
CREATE TABLE warsztat.Pojazd (
  VIN CHAR(17) NOT NULL,
  Rejestracja CHAR(8) NOT NULL,
  Rok SMALLINT NOT NULL,
  Marka VARCHAR(20) NOT NULL,
  Model VARCHAR(20) NOT NULL,
  Klient_PESEL CHAR(11) NOT NULL,
  PRIMARY KEY (VIN, Klient_PESEL),
  CONSTRAINT WLASNOSC
    FOREIGN KEY (Klient_PESEL)
    REFERENCES warsztat.klient(PESEL) 
    ON DELETE CASCADE
    ON UPDATE CASCADE);

CREATE INDEX WLASNOSC_INDEX ON warsztat.Pojazd (Klient_PESEL ASC);

-- -----------------------------------------------------
-- Tabela Pracownik
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS warsztat.Pracownik (
  PESEL CHAR(11) NOT NULL,
  Imie VARCHAR(20) NOT NULL,
  Nazwisko VARCHAR(40) NOT NULL,
  Telefon INT NOT NULL,
  Email VARCHAR(30) NULL,
  PRIMARY KEY (PESEL));
CREATE UNIQUE INDEX PESEL_UNIQUE_INDEX ON warsztat.Pracownik (PESEL ASC);
CREATE INDEX IMIE_NAZWISKO_INDEX ON warsztat.Pracownik (imie, nazwisko);


-- -----------------------------------------------------
-- Tabela Naprawa
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS warsztat.Naprawa (
  idNaprawa SMALLINT NOT NULL AUTO_INCREMENT,
  Pojazd_VIN CHAR(17) NOT NULL,
  Data_przyjecia DATE NOT NULL,
  Data_oddania DATE NULL,
  Zgloszone_usterki VARCHAR(500) NOT NULL,
  Przeprowadzone_naprawy VARCHAR(500) NULL,
  Koszt_napraw FLOAT NULL,
  Koszt_czesci FLOAT NULL,
  Paliwo_przed SMALLINT NOT NULL,
  Paliwo_po SMALLINT NULL,
  Pracownik_PESEL CHAR(11) NOT NULL,
  PRIMARY KEY (idNaprawa, Pojazd_VIN, Pracownik_PESEL),
  CONSTRAINT WIZYTA
    FOREIGN KEY (Pojazd_VIN)
    REFERENCES warsztat.Pojazd (VIN)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT ODPOWIEDZIALNOSC
    FOREIGN KEY (Pracownik_PESEL)
    REFERENCES warsztat.Pracownik (PESEL)
    ON DELETE CASCADE
    ON UPDATE CASCADE);

CREATE INDEX WIZYTA_INDEX ON warsztat.Naprawa (Pojazd_VIN ASC);
CREATE INDEX ODPOWIEDZIALNOSC_INDEX ON warsztat.Naprawa (Pracownik_PESEL ASC);
CREATE UNIQUE INDEX idNaprawa_UNIQUE_INDEX ON warsztat.Naprawa (idNaprawa ASC);


-- -----------------------------------------------------
-- INSERTy dla Klient
-- -----------------------------------------------------

INSERT INTO warsztat.Klient (PESEL, Imie, Nazwisko, Telefon, Email, NIP) VALUES ('95010233333', 'Jan', 'Kowalski', 123456458, 'jkowalski@mail.com', '3213121244');
INSERT INTO warsztat.Klient (PESEL, Imie, Nazwisko, Telefon, Email, NIP) VALUES ('94010233333', 'Cezary', 'Pazura', 653213458, 'czarek@mail.com', '0013121244');
INSERT INTO warsztat.Klient (PESEL, Imie, Nazwisko, Telefon, Email) VALUES ('93010233333', 'Stanisław', 'Brzytwa', 754213458, 'stasiekszarp@mail.com');
INSERT INTO warsztat.Klient (PESEL, Imie, Nazwisko, Telefon) VALUES ('92010233333', 'Kazimierz', 'Wielki', 954213458);
INSERT INTO warsztat.Klient (PESEL, Imie, Nazwisko, Telefon) VALUES ('91010233333', 'Tadeusz', 'Soplica', 254213458);
INSERT INTO warsztat.Klient (PESEL, Imie, Nazwisko, Telefon) VALUES ('90010233333', 'Adam', 'Miałczyński', 354213458);



-- -----------------------------------------------------
-- INSERTy dla Pojazd
-- -----------------------------------------------------

INSERT INTO warsztat.Pojazd (VIN, Rejestracja, Rok, Marka, Model, Klient_PESEL) VALUES ('WVWZZZ1KZ7W617317', 'EBE2263', 2000, 'Lada', 'Samara', '95010233333');
INSERT INTO warsztat.Pojazd (VIN, Rejestracja, Rok, Marka, Model, Klient_PESEL) VALUES ('WVWZZZ1KZ7W123317', 'EBE1163', 1999, 'Fiat', 'Bravo HGT', '94010233333');
INSERT INTO warsztat.Pojazd (VIN, Rejestracja, Rok, Marka, Model, Klient_PESEL) VALUES ('WVWZZZ1AWQR411317', 'ERA2263', 1995, 'Ford', 'Mondeo', '93010233333');
INSERT INTO warsztat.Pojazd (VIN, Rejestracja, Rok, Marka, Model, Klient_PESEL) VALUES ('VOLOZZ1KZ7W617317', 'DWR99999', 1985, 'Renault', '25', '92010233333');
INSERT INTO warsztat.Pojazd (VIN, Rejestracja, Rok, Marka, Model, Klient_PESEL) VALUES ('WVWZZZ1KZ7W614444', 'EZG1233', 1988, 'Renaut', '21', '91010233333');
INSERT INTO warsztat.Pojazd (VIN, Rejestracja, Rok, Marka, Model, Klient_PESEL) VALUES ('WVWZZZ1KZGHU21123', 'DW22634', 2000, 'Opel', 'Omega', '90010233333');
INSERT INTO warsztat.Pojazd (VIN, Rejestracja, Rok, Marka, Model, Klient_PESEL) VALUES ('WVWZZ123213FF2311', 'EBE1351', 2000, 'Skoda', 'Octavia', '95010233333');

-- -----------------------------------------------------
-- INSERTy dla Pracownik
-- -----------------------------------------------------

INSERT INTO warsztat.Pracownik (PESEL, Imie, Nazwisko, Telefon, Email) VALUES ('92060623333', 'Robert', 'Kupicha', 876123004, 'robk@mail.com');
INSERT INTO warsztat.Pracownik (PESEL, Imie, Nazwisko, Telefon, Email) VALUES ('73121223241', 'Marian', 'Mądry', 456123004, 'mozd@mail.com');
INSERT INTO warsztat.Pracownik (PESEL, Imie, Nazwisko, Telefon, Email) VALUES ('55091726423', 'Siergiej', 'Romanov', 123123004, 'siergiej.romanov.323@mail.ru');

-- -----------------------------------------------------
-- INSERTy dla Naprawa
-- -----------------------------------------------------

INSERT INTO warsztat.Naprawa (Pojazd_VIN, Data_przyjecia, Data_oddania, Zgloszone_usterki, Przeprowadzone_naprawy, Koszt_napraw, Koszt_czesci, Paliwo_przed, Paliwo_po, Pracownik_PESEL) 
VALUES ('WVWZZZ1KZ7W617317', '2015-01-12', '2018-12-24', 'Szarpie na zimnym, gaśnie.', 'Wymiana czujnika temperatury cieczy', 20, 55, 100, 20, '92060623333');

INSERT INTO warsztat.Naprawa (Pojazd_VIN, Data_przyjecia, Data_oddania, Zgloszone_usterki, Przeprowadzone_naprawy, Koszt_napraw, Koszt_czesci, Paliwo_przed, Paliwo_po, Pracownik_PESEL) 
VALUES ('WVWZZZ1KZ7W123317', '2016-01-12', '2018-12-24', 'Nie odpala.', 'Wymiana rozrusznika', 50, 300, 100, 20, '73121223241');

INSERT INTO warsztat.Naprawa (Pojazd_VIN, Data_przyjecia, Data_oddania, Zgloszone_usterki, Przeprowadzone_naprawy, Koszt_napraw, Koszt_czesci, Paliwo_przed, Paliwo_po, Pracownik_PESEL) 
VALUES ('WVWZZZ1AWQR411317', '2017-11-12', '2018-12-24', 'Urwany wydech', 'Nowy wydech', 20, 55, 22, 20, '55091726423');

INSERT INTO warsztat.Naprawa (Pojazd_VIN, Data_przyjecia, Data_oddania, Zgloszone_usterki, Przeprowadzone_naprawy, Koszt_napraw, Koszt_czesci, Paliwo_przed, Paliwo_po, Pracownik_PESEL) 
VALUES ('VOLOZZ1KZ7W617317', '2017-11-19', '2018-12-24', 'Bierze olej', 'Nowe pierścienie', 20, 55, 50, 50, '55091726423');

CREATE VIEW warsztat.NaprawaUproszczony AS
	SELECT
		warsztat.naprawa.idNaprawa AS ID,
		warsztat.klient.Imie, 
        warsztat.klient.Nazwisko, 
		warsztat.pojazd.Rejestracja, 
        warsztat.pojazd.Marka, 
        warsztat.pojazd.Model, 
		warsztat.naprawa.Data_przyjecia, 
        warsztat.naprawa.Data_oddania
	FROM
		warsztat.klient, warsztat.pojazd, warsztat.naprawa
	WHERE
		warsztat.pojazd.Klient_PESEL = warsztat.klient.PESEL 
        AND warsztat.naprawa.Pojazd_VIN = warsztat.pojazd.VIN;

CREATE VIEW warsztat.NaprawaSzczegoly AS
	SELECT
		warsztat.naprawa.idNaprawa AS ID,
		warsztat.klient.Imie AS Imie_Klient,
        warsztat.klient.Nazwisko AS Nazwisko_Klient, 
		warsztat.pojazd.Rejestracja,
        warsztat.pojazd.Marka,
        warsztat.pojazd.Model, 
		warsztat.naprawa.Data_przyjecia,
        warsztat.naprawa.Data_oddania,
        warsztat.naprawa.Zgloszone_usterki,
        warsztat.naprawa.Przeprowadzone_naprawy,
        warsztat.naprawa.Koszt_napraw,
        warsztat.naprawa.Koszt_czesci,
        warsztat.naprawa.Paliwo_przed,
        warsztat.naprawa.Paliwo_po,
        warsztat.pracownik.Imie AS Imie_Pracownik,
        warsztat.pracownik.Nazwisko AS Nazwisko_Pracownik   
        
	FROM
		warsztat.klient, warsztat.pojazd, warsztat.naprawa, warsztat.pracownik
	WHERE
		warsztat.pojazd.Klient_PESEL = warsztat.klient.PESEL 
        AND warsztat.naprawa.Pojazd_VIN = warsztat.pojazd.VIN
        AND warsztat.naprawa.Pracownik_PESEL = warsztat.pracownik.PESEL;


DROP USER IF EXISTS 'admin'@'localhost';
CREATE USER 'admin'@'localhost' IDENTIFIED BY 'admin'; 
GRANT ALL PRIVILEGES ON * . * TO 'admin'@'localhost';
FLUSH PRIVILEGES;

DROP USER IF EXISTS 'pracownik'@'localhost';
CREATE USER 'pracownik'@'localhost' IDENTIFIED BY 'pracownik'; 
GRANT SELECT, INSERT, UPDATE ON warsztat.klient TO 'pracownik'@'localhost';
GRANT SELECT, INSERT, UPDATE ON warsztat.pojazd TO 'pracownik'@'localhost';
GRANT SELECT, INSERT, UPDATE ON warsztat.naprawa TO 'pracownik'@'localhost';
GRANT SELECT				 ON warsztat.naprawauproszczony TO 'pracownik'@'localhost';
GRANT SELECT				 ON warsztat.naprawaszczegoly TO 'pracownik'@'localhost';
GRANT SELECT				 ON warsztat.pracownik TO 'pracownik'@'localhost';
FLUSH PRIVILEGES;



