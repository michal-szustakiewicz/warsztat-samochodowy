package sample;

import javax.sql.rowset.CachedRowSet;
import java.sql.*;

import javax.sql.rowset.RowSetProvider;


public class DBConnection {

    private Connection connection = null;

    private static String loggedUser = "";
    private static String connectionString = "";

    public static boolean setConnectionString(String login, String password) {
        connectionString = "jdbc:mysql://localhost/warsztat?user="+login+"&password="+password+"&useSSL=false&useLegacyDatetimeCode=false&serverTimezone=Europe/Warsaw&useSSL=false";
        Connection connection = null;
        //połączenie z bazą
        try {
            connection = DriverManager.getConnection(connectionString);
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
            loggedUser = login;
            return true;

        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
            return false;
        }
    }

    public static String getLoggedUser() {
        return loggedUser;
    }

    public void connect(){
        //połączenie z bazą
        try {
            connection = DriverManager.getConnection(connectionString);
        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
    }

    public void disconnect(){
        try {
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }

        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
    }


    public void execute(String command) throws SQLException, ClassNotFoundException {

        Statement stmt = null;
        try {
            stmt = connection.createStatement();
            stmt.execute(command);

        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        } finally {
            if (stmt != null) {
                //Close statement
                stmt.close();
            }
            //Close connection
        }
    }

    public ResultSet getDataFromDB(String command) throws SQLException {

        Statement stmt = null;
        ResultSet rs = null;
        CachedRowSet cachedRowSet = RowSetProvider.newFactory().createCachedRowSet();

        //Wysłanie komendy SQL i odebranie rezultatu
        try {

            stmt = connection.createStatement();

            if (stmt.execute(command)) {
                rs = stmt.getResultSet();
                cachedRowSet.populate(rs);
            }

        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        } finally {



            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException sqlEx) {
                }

                stmt = null;
            }
        }
        return cachedRowSet;

    }

    public static Date getDateFromDB(ResultSet rs, String columnName) throws SQLException {
        String result = rs.getString(columnName);
        if(result == null) return null;
        else return Date.valueOf(result);
    }

    public static Float getFloatFromDB(ResultSet rs, String columnName) throws SQLException {
        String result = rs.getString(columnName);
        if(result == null) return null;
        else return Float.valueOf(result);
    }

    public static Integer getIntegerFromDB(ResultSet rs, String columnName) throws SQLException {
        String result = rs.getString(columnName);
        if(result == null) return null;
        else return Integer.valueOf(result);
    }


    public static String DateToString(Date date) throws SQLException {
        if(date == null) return null;
        else return "'"+date.toString()+"'";
    }

    public static String FloatToString(Float f) throws SQLException {
        if(f == null) return null;
        else return f.toString();
    }

    public static Float FloatFromString(String f) throws SQLException {
        if(f == null || f.isEmpty()) return null;
        else return Float.valueOf(f);
    }

    public static String IntegerToString(Integer i) throws SQLException {
        if(i == null) return null;
        else return i.toString();
    }

    public static Integer IntegerFromString(String i) throws SQLException {
        if(i == null || i.isEmpty()) return null;
        else return Integer.valueOf(i);
    }

}
