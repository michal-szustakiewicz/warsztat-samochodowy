package sample;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Pracownik {
    private String PESEL;
    private String Imie;
    private String Nazwisko;
    private int Telefon;
    private String Email;
    private String oldPESEL;



    public Pracownik(String PESEL, String imie, String nazwisko, int telefon, String email) {
        this.PESEL = PESEL;
        Imie = imie;
        Nazwisko = nazwisko;
        Telefon = telefon;
        Email = email;
    }

    public Pracownik() {

    }

    public void insertIntoDB(DBConnection dbConnection) throws SQLException, ClassNotFoundException {
        String command = "INSERT INTO `warsztat`.`Pracownik` (`PESEL`, `Imie`, `Nazwisko`, `Telefon`, `Email`) VALUES ('"
                + PESEL+"' ,'"+ Imie+"' ,'"+ Nazwisko+"' ,"+ Telefon+" ,'"+ Email+"')";
        dbConnection.execute(command);
    }

    public void updateDB(DBConnection dbConnection) throws SQLException, ClassNotFoundException {
        String command = "UPDATE warsztat.pracownik SET PESEL = '"+PESEL+"', Imie = '"+Imie+"', Nazwisko = '"+Nazwisko+"', Telefon = "+Telefon+", Email = '"+Email+"' WHERE PESEL = "+oldPESEL;
        dbConnection.execute(command);
    }

    public void deleteFromDB(DBConnection dbConnection) throws SQLException, ClassNotFoundException {
        String command = "DELETE FROM warsztat.pracownik WHERE PESEL = "+PESEL;
        dbConnection.execute(command);
    }

    public static ArrayList<Pracownik> getList(DBConnection dbConnection, int limit) throws SQLException, ClassNotFoundException {
        ArrayList<Pracownik> lista = new ArrayList<>();

        String command = "SELECT * FROM warsztat.pracownik";
        if(limit>0) command+=" LIMIT "+limit;
        ResultSet rs = dbConnection.getDataFromDB(command);


        try {

            while (rs.next()) {
                lista.add(new Pracownik(rs.getString("PESEL"),rs.getString("Imie"),
                        rs.getString("Nazwisko"),Integer.parseInt(rs.getString("Telefon")),
                        rs.getString("Email")));
            }

        }
        catch (SQLException ex){
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }

        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException sqlEx) {
            }
        }

        return lista;
    }

    public static ArrayList<NaprawaUproszczony> getNaprawy(DBConnection dbConnection, String PESEL ,int limit) throws SQLException {
        ArrayList<NaprawaUproszczony> lista = new ArrayList<>();

        String command = "SELECT warsztat.naprawauproszczony.* " +
                "FROM " +
                "warsztat.naprawauproszczony, warsztat.naprawa, warsztat.pracownik " +
                "WHERE " +
                "warsztat.naprawauproszczony.ID = warsztat.naprawa.idNaprawa " +
                "AND warsztat.pracownik.PESEL = warsztat.naprawa.Pracownik_PESEL " +
                "AND warsztat.pracownik.PESEL = '"+PESEL+"'";

        if(limit>0) command+=" LIMIT "+limit;
        ResultSet rs = dbConnection.getDataFromDB(command);

        try {

            while (rs.next()) {
                lista.add(new NaprawaUproszczony(Integer.parseInt(rs.getString("ID")),
                        rs.getString("Imie"),rs.getString("Nazwisko"),rs.getString("Rejestracja"),
                        rs.getString("Marka"),rs.getString("Model"),
                        DBConnection.getDateFromDB(rs,"Data_przyjecia"),DBConnection.getDateFromDB(rs,"Data_oddania")));
            }

        }
        catch (SQLException ex){
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }

        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException sqlEx) {
            }
        }

        return lista;
    }

    public static ArrayList<Pracownik> searchFor(DBConnection dbConnection, int limit,String text) throws SQLException {
        ArrayList<Pracownik> lista = new ArrayList<>();

        String[] tmp = text.split(" ");

        String command = "";
        if(text.isEmpty()) command = "SELECT * FROM warsztat.pracownik";
        else if(tmp.length == 1) command = "SELECT * FROM warsztat.pracownik WHERE Imie LIKE '"+tmp[0]+"%' OR Nazwisko LIKE '"+tmp[0]+"%'";
        else if(tmp.length == 2) command = "SELECT * FROM warsztat.pracownik WHERE Imie LIKE '"+tmp[0]+"' AND Nazwisko LIKE '"+tmp[1]+"%'";

        if(limit>0) command+=" LIMIT "+limit;
        ResultSet rs = dbConnection.getDataFromDB(command);
        try {

            while (rs.next()) {
                lista.add(new Pracownik(rs.getString("PESEL"),rs.getString("Imie"),
                        rs.getString("Nazwisko"),Integer.parseInt(rs.getString("Telefon")),
                        rs.getString("Email")));
            }

        }
        catch (SQLException ex){
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }

        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException sqlEx) {
            }
        }
        return lista;
    }

    @Override
    public String toString() {
        return "['"+ PESEL+"' ,'"+ Imie+"' ,'"+ Nazwisko+"' ,"+ Telefon+" ,'"+ Email+"'];";
    }

    public String getPESEL() {
        return PESEL;
    }

    public void setPESEL(String PESEL) {
        this.PESEL = PESEL;
    }

    public String getImie() {
        return Imie;
    }

    public void setImie(String imie) {
        Imie = imie;
    }

    public String getNazwisko() {
        return Nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        Nazwisko = nazwisko;
    }

    public int getTelefon() {
        return Telefon;
    }

    public void setTelefon(int telefon) {
        Telefon = telefon;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getOldPESEL() {
        return oldPESEL;
    }

    public void setOldPESEL(String oldPESEL) {
        this.oldPESEL = oldPESEL;
    }
}
