package sample;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class NaprawaUproszczony {

    private int idNaprawa;
    private String Imie;
    private String Nazwisko;
    private String Rejestracja;
    private String Marka;
    private String Model;
    private Date Data_przyjecia;
    private Date Data_oddania;

    public NaprawaUproszczony(int idNaprawa, String imie, String nazwisko, String rejestracja,
                              String marka, String model, Date data_przyjecia, Date data_oddania) {
        this.idNaprawa = idNaprawa;
        Imie = imie;
        Nazwisko = nazwisko;
        Rejestracja = rejestracja;
        Marka = marka;
        Model = model;
        Data_przyjecia = data_przyjecia;
        Data_oddania = data_oddania;
    }

    public static ArrayList<NaprawaUproszczony> getList(DBConnection dbConnection, int limit) throws SQLException {
        ArrayList<NaprawaUproszczony> lista = new ArrayList<>();

        String command = "SELECT * FROM warsztat.naprawauproszczony";
        if(limit>0) command+=" LIMIT "+limit;
        ResultSet rs = dbConnection.getDataFromDB(command);

        try {

            while (rs.next()) {
                lista.add(new NaprawaUproszczony(Integer.parseInt(rs.getString("ID")),
                        rs.getString("Imie"),rs.getString("Nazwisko"),rs.getString("Rejestracja"),
                        rs.getString("Marka"),rs.getString("Model"),
                        DBConnection.getDateFromDB(rs,"Data_przyjecia"),DBConnection.getDateFromDB(rs,"Data_oddania")));
            }

        }
        catch (SQLException ex){
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }

        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException sqlEx) {
            }
        }

        return lista;
    }

    public Naprawa getNaprawaFromDB(DBConnection dbConnection) throws SQLException {
        Naprawa naprawa = new Naprawa();

        String command = "SELECT * " +
                "FROM " +
                "warsztat.naprawa " +
                "WHERE " +
                "warsztat.naprawa.idNaprawa = "+idNaprawa;

        ResultSet rs = dbConnection.getDataFromDB(command);

        try {

            while (rs.next()) {
                naprawa = new Naprawa(Integer.parseInt(rs.getString("idNaprawa")),rs.getString("Pojazd_VIN"),DBConnection.getDateFromDB(rs,"Data_przyjecia"),
                        DBConnection.getDateFromDB(rs,"Data_oddania"),rs.getString("Zgloszone_usterki"),rs.getString("Przeprowadzone_naprawy"),
                        DBConnection.getFloatFromDB(rs,"Koszt_napraw"),DBConnection.getFloatFromDB(rs,"Koszt_czesci"),
                        Integer.parseInt(rs.getString("Paliwo_przed")),DBConnection.getIntegerFromDB(rs,"Paliwo_po"),
                        rs.getString("Pracownik_PESEL"));
            }

        }
        catch (SQLException ex){
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }

        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException sqlEx) {
            }
        }

        return naprawa;
    }

    @Override
    public String toString() {
        return "['ID:"+idNaprawa+", '"+Imie+"', '"+Nazwisko+"', '"+Rejestracja+"', '"+Marka+"', '"+Model+"', "+Data_przyjecia
                +", "+Data_oddania+"'];";
    }

    public int getIdNaprawa() {
        return idNaprawa;
    }

    public void setIdNaprawa(int idNaprawa) {
        this.idNaprawa = idNaprawa;
    }

    public String getImie() {
        return Imie;
    }

    public void setImie(String imie) {
        Imie = imie;
    }

    public String getNazwisko() {
        return Nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        Nazwisko = nazwisko;
    }

    public String getRejestracja() {
        return Rejestracja;
    }

    public void setRejestracja(String rejestracja) {
        Rejestracja = rejestracja;
    }

    public String getMarka() {
        return Marka;
    }

    public void setMarka(String marka) {
        Marka = marka;
    }

    public String getModel() {
        return Model;
    }

    public void setModel(String model) {
        Model = model;
    }

    public Date getData_przyjecia() {
        return Data_przyjecia;
    }

    public void setData_przyjecia(Date data_przyjecia) {
        Data_przyjecia = data_przyjecia;
    }

    public Date getData_oddania() {
        return Data_oddania;
    }

    public void setData_oddania(Date data_oddania) {
        Data_oddania = data_oddania;
    }
}
