package sample;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

public class Naprawa{

    private int idNaprawa = 0;
    private String Pojazd_VIN;
    private Date Data_przyjecia;
    private Date Data_oddania;
    private String Zgłoszone_usterki;
    private String Przeprowadzone_naprawy;
    private Float Koszt_napraw;
    private Float Koszt_czesci;
    private int Paliwo_przed;
    private Integer Paliwo_po;
    private String Pracownik_PESEL;

    public Naprawa(int idNaprawa , String pojazd_VIN, Date data_przyjecia, Date data_oddania, String zgłoszone_usterki, String przeprowadzone_naprawy, Float koszt_napraw, Float koszt_czesci, int paliwo_przed, Integer paliwo_po, String pracownik_PESEL) {
        this.idNaprawa = idNaprawa;
        Pojazd_VIN = pojazd_VIN;
        Data_przyjecia = data_przyjecia;
        Data_oddania = data_oddania;
        Zgłoszone_usterki = zgłoszone_usterki;
        Przeprowadzone_naprawy = przeprowadzone_naprawy;
        Koszt_napraw = koszt_napraw;
        Koszt_czesci = koszt_czesci;
        Paliwo_przed = paliwo_przed;
        Paliwo_po = paliwo_po;
        Pracownik_PESEL = pracownik_PESEL;
    }

    public Naprawa() {
    }

    public void insertIntoDB(DBConnection dbConnection) throws SQLException, ClassNotFoundException {


        String command = "INSERT INTO `warsztat`.`Naprawa` (`Pojazd_VIN`, `Data_przyjecia`, `Data_oddania`, `Zgloszone_usterki`, `Przeprowadzone_naprawy`"+
                ", `Koszt_napraw`, `Koszt_czesci`, `Paliwo_przed`, `Paliwo_po`, `Pracownik_PESEL`) VALUES ('"
                +Pojazd_VIN+"', '"+Data_przyjecia+"', "+Data_oddania+", '"+Zgłoszone_usterki+"', "
                +Przeprowadzone_naprawy+", "+Koszt_napraw+", "+Koszt_czesci+", "+Paliwo_przed+", "
                +Paliwo_po+", '"+Pracownik_PESEL+"');";

        dbConnection.execute(command);
    }

    public void updateDB(DBConnection dbConnection) throws SQLException, ClassNotFoundException {

        String command = "UPDATE `warsztat`.`naprawa`  SET Pojazd_VIN = '"+Pojazd_VIN+"', Data_przyjecia = '"+Data_przyjecia.toString()+
                "', Data_oddania = "+DBConnection.DateToString(Data_oddania)+", Zgloszone_usterki = '"+Zgłoszone_usterki+"', Przeprowadzone_naprawy = "+Przeprowadzone_naprawy+
                ", Koszt_napraw = "+Koszt_napraw+", Koszt_czesci = "+Koszt_czesci+
                ", Paliwo_przed = "+Paliwo_przed+", Paliwo_po = "+Paliwo_po+" WHERE idNaprawa = '"+idNaprawa+"'";
        dbConnection.execute(command);
    }

    public void deleteFromDB(DBConnection dbConnection) throws SQLException, ClassNotFoundException {
        String command = "DELETE FROM warsztat.naprawa WHERE idNaprawa = "+idNaprawa;
        dbConnection.execute(command);
    }

    public static ArrayList<Naprawa> getList(DBConnection dbConnection, int limit) throws SQLException, ClassNotFoundException {
        ArrayList<Naprawa> lista = new ArrayList<>();

        String command = "SELECT * FROM naprawa.naprawa";
        if(limit>0) command+=" LIMIT "+limit;
        ResultSet rs = dbConnection.getDataFromDB(command);

        try {

            while (rs.next()) {
                lista.add(new Naprawa(Integer.parseInt(rs.getString("idNaprawa")),rs.getString("Pojazd_VIN"),rs.getDate("Data_przyjecia"),
                        rs.getDate("Data_oddania"),rs.getString("Zgloszone_usterki"),rs.getString("Przeprowadzone_naprawy"),
                        DBConnection.getFloatFromDB(rs,"Koszt_napraw"), DBConnection.getFloatFromDB(rs,"Koszt_czesci"),
                        Integer.parseInt(rs.getString("Paliwo_przed")),DBConnection.getIntegerFromDB(rs,"Paliwo_po"),
                        rs.getString("Pracownik_PESEL")));
            }

        }
        catch (SQLException ex){
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }

        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException sqlEx) {
            }
        }

        return lista;
    }

    public static ArrayList<NaprawaUproszczony> searchFor(DBConnection dbConnection, LocalDate dataPrzyjeciaOd, LocalDate dataPrzyjeciaDo , int limit) throws SQLException {
        ArrayList<NaprawaUproszczony> lista = new ArrayList<>();

        String command ="";
        //nic oba nulle
        if(dataPrzyjeciaOd == null && dataPrzyjeciaDo == null) {
            return lista;
        }
        //data od >
        else if(dataPrzyjeciaOd != null && dataPrzyjeciaDo == null) {
                command = "SELECT warsztat.naprawauproszczony.*" +
                        "FROM " +
                        "warsztat.naprawauproszczony " +
                        "WHERE " +
                        "warsztat.naprawauproszczony.Data_przyjecia >= '"+dataPrzyjeciaOd.toString()+"'";
        }
        //data do <
        else if(dataPrzyjeciaOd == null && dataPrzyjeciaDo != null) {
            command = "SELECT warsztat.naprawauproszczony.*" +
                    "FROM " +
                    "warsztat.naprawauproszczony " +
                    "WHERE " +
                    "warsztat.naprawauproszczony.Data_przyjecia <= '"+dataPrzyjeciaDo.toString()+"'";
        }
        //data od > do
        else if(dataPrzyjeciaOd != null && dataPrzyjeciaDo != null) {
            command = "SELECT warsztat.naprawauproszczony.*" +
                    "FROM " +
                    "warsztat.naprawauproszczony " +
                    "WHERE " +
                    "warsztat.naprawauproszczony.Data_przyjecia >= '"+dataPrzyjeciaOd.toString()+"' "+
                    "AND warsztat.naprawauproszczony.Data_przyjecia <= '"+dataPrzyjeciaDo.toString()+"'";

        }

        if(limit>0) command+=" LIMIT "+limit;
        ResultSet rs = dbConnection.getDataFromDB(command);

        try {

            while (rs.next()) {
                lista.add(new NaprawaUproszczony(Integer.parseInt(rs.getString("ID")),
                        rs.getString("Imie"),rs.getString("Nazwisko"),rs.getString("Rejestracja"),
                        rs.getString("Marka"),rs.getString("Model"),
                        DBConnection.getDateFromDB(rs,"Data_przyjecia"),DBConnection.getDateFromDB(rs,"Data_oddania")));
            }

        }
        catch (SQLException ex){
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }

        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException sqlEx) {
            }
        }

        return lista;
    }

    public int getIdNaprawa() {
        return idNaprawa;
    }

    public void setIdNaprawa(int idNaprawa) {
        this.idNaprawa = idNaprawa;
    }

    public static Pojazd getPojazd(DBConnection dbConnection, int idNaprawa) throws SQLException {
        Pojazd pojazd = new Pojazd();

        String command = "SELECT warsztat.pojazd.* FROM warsztat.pojazd, warsztat.naprawa, warsztat.naprawauproszczony "+
                "WHERE warsztat.pojazd.VIN = warsztat.naprawa.Pojazd_VIN AND warsztat.naprawa.idNaprawa = warsztat.naprawauproszczony.ID "+
                "AND warsztat.naprawauproszczony.ID = "+idNaprawa;

        ResultSet rs = dbConnection.getDataFromDB(command);

        try {
            while (rs.next()) {
                pojazd = new Pojazd(rs.getString("VIN"),rs.getString("Rejestracja"),
                        Integer.parseInt(rs.getString("Rok")),
                        rs.getString("Marka"),rs.getString("Model"),rs.getString("Klient_PESEL"));
            }

        }
        catch (SQLException ex){
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }

        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException sqlEx) {
            }
        }

        return pojazd;
    }

    public static Pracownik getPracownik(DBConnection dbConnection, int idNaprawa) throws SQLException {
        Pracownik pracownik = new Pracownik();

        String command = "SELECT warsztat.pracownik.* FROM warsztat.pracownik, warsztat.naprawa, warsztat.naprawauproszczony "+
                "WHERE warsztat.pracownik.PESEL = warsztat.naprawa.Pracownik_PESEL AND warsztat.naprawa.idNaprawa = warsztat.naprawauproszczony.ID "+
                "AND warsztat.naprawauproszczony.ID = "+idNaprawa;

        ResultSet rs = dbConnection.getDataFromDB(command);

        try {
            while (rs.next()) {
                pracownik = new Pracownik(rs.getString("PESEL"),rs.getString("Imie"),
                        rs.getString("Nazwisko"),Integer.parseInt(rs.getString("Telefon")),
                        rs.getString("Email"));
            }

        }
        catch (SQLException ex){
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }

        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException sqlEx) {
            }
        }

        return pracownik;
    }

    public static Klient getKlient(DBConnection dbConnection, int idNaprawa) throws SQLException {
        Klient klient = new Klient();

        String command = "SELECT warsztat.klient.* FROM warsztat.klient, warsztat.pojazd, warsztat.naprawa, warsztat.naprawauproszczony "+
                "WHERE warsztat.klient.PESEL = warsztat.pojazd.Klient_PESEL "+
                "AND warsztat.pojazd.VIN = warsztat.naprawa.Pojazd_VIN "+
                "AND warsztat.naprawa.idNaprawa = warsztat.naprawauproszczony.ID "+
                "AND warsztat.naprawauproszczony.ID = "+idNaprawa;

        ResultSet rs = dbConnection.getDataFromDB(command);

        try {
            while (rs.next()) {
                klient = new Klient(rs.getString("PESEL"),rs.getString("Imie"),
                        rs.getString("Nazwisko"),Integer.parseInt(rs.getString("Telefon")),
                        rs.getString("Email"),rs.getString("NIP"));
            }

        }
        catch (SQLException ex){
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }

        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException sqlEx) {
            }
        }

        return klient;
    }
    @Override
    public String toString() {
        return "['ID:"+idNaprawa+", '"+Pojazd_VIN+"', '"+Data_przyjecia+"', '"+Data_oddania+"', '"+Zgłoszone_usterki+"', '"+Przeprowadzone_naprawy+"', "+Koszt_napraw+", "+Koszt_czesci+", "+Paliwo_przed+", "+Paliwo_po+", '"+Pracownik_PESEL+"'];";
    }

    public String getPojazd_VIN() {
        return Pojazd_VIN;
    }

    public void setPojazd_VIN(String pojazd_VIN) {
        Pojazd_VIN = pojazd_VIN;
    }

    public Date getData_przyjecia() {
        return Data_przyjecia;
    }

    public void setData_przyjecia(Date data_przyjecia) {
        Data_przyjecia = data_przyjecia;
    }

    public Date getData_oddania() {
        return Data_oddania;
    }

    public void setData_oddania(Date data_oddania) {
        Data_oddania = data_oddania;
    }

    public String getZgłoszone_usterki() {
        return Zgłoszone_usterki;
    }

    public void setZgłoszone_usterki(String zgłoszone_usterki) {
        Zgłoszone_usterki = zgłoszone_usterki;
    }

    public String getPrzeprowadzone_naprawy() {
        return Przeprowadzone_naprawy;
    }

    public void setPrzeprowadzone_naprawy(String przeprowadzone_naprawy) {
        Przeprowadzone_naprawy = przeprowadzone_naprawy;
    }

    public Float getKoszt_napraw() {
        return Koszt_napraw;
    }

    public void setKoszt_napraw(Float koszt_napraw) {
        Koszt_napraw = koszt_napraw;
    }

    public Float getKoszt_czesci() {
        return Koszt_czesci;
    }

    public void setKoszt_czesci(Float koszt_czesci) {
        Koszt_czesci = koszt_czesci;
    }

    public int getPaliwo_przed() {
        return Paliwo_przed;
    }

    public void setPaliwo_przed(int paliwo_przed) {
        Paliwo_przed = paliwo_przed;
    }

    public Integer getPaliwo_po() {
        return Paliwo_po;
    }

    public void setPaliwo_po(int paliwo_po) {
        Paliwo_po = paliwo_po;
    }

    public String getPracownik_PESEL() {
        return Pracownik_PESEL;
    }

    public void setPracownik_PESEL(String pracownik_PESEL) {
        Pracownik_PESEL = pracownik_PESEL;
    }
}
